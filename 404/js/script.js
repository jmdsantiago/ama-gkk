$(function() {
    function e() {
        $(".panda-404, .panda-maintenance").removeAttr("style");
        windowsHeight = $(window).height();
        p404Height = $(".panda-404").height();
        maintenanceHeight = $(".panda-maintenance").height();
        if (p404Height < windowsHeight - 50) {
            $(".panda-404").css({
                height: windowsHeight,
                "padding-top": (windowsHeight - 16 - p404Height) / 2
            });
        }
        if (maintenanceHeight < windowsHeight - $("footer").height()) {
            $(".panda-maintenance").css({
                height: windowsHeight - $("footer").height(),
                "padding-top": (windowsHeight - 16 - maintenanceHeight) / 2
            });
        }
    }
    $(document).ready(function() {
        if ($(".panda-countdown").length) {
            var t = new Date();
            t = new Date(t.getFullYear(), t.getMonth(), t.getDate() + 1, 9, 10, 0);
            var n = new Date();
            var r = t.getTime() / 1e3 - n.getTime() / 1e3;
            var i = $(".panda-countdown").FlipClock(r, {
                clockFace: "DailyCounter",
                countdown: true
            });
        }
        if ($("#menu").length) {
            $("#menu").slicknav({
                prependTo: "#responsivemenu"
            });
        }
        e();
    });
    $(window).load(function() {
        e();
    });
    $(window).resize(function() {
        e();
    });
});