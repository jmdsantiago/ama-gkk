<?php
session_start();
if(!isset($_SESSION['usr_num'])) {
	header("Location: /login");
}
include $_SERVER['DOCUMENT_ROOT'] . '/top_page_comment.html';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<script src="/assets/preloader/pace.min.js"></script>
		<link rel="stylesheet" href="/assets/preloader/pace.css">
		<meta charset="utf-8">
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="Classified. Access denied." />
		<meta name="author" content="John Santiago" />
		<title>Questionnaire | 001A</title>
		<?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/important_scripts.php'; ?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->

	</head>
	<body class="page-body page-fade-only">
		<div class="page-container horizontal-menu">
			<!-- SIDEBAR -->
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/header.php'; ?>
			<div class="main-content">
				<!--PLACE ALERTS HERE-->
				<?php include $_SERVER['DOCUMENT_ROOT'] . '/alerts.php'; ?>
                <h3>Rankings online...</h3>
                <div class="row">
                    <div class="col-md-9" id="max_entries_user"></div>
                    <div class="col-md-2">
                        <img src="/assets/images/yay.gif" height="150">
                    </div>
                </div>
                <br>
                <br>
				<ol class="breadcrumb bc-3">
					<li>
						<a href="/wizard"><i class="entypo-home"></i>Home</a>
					</li>
					<li class="active">
						<strong>Form</strong>
					</li>
				</ol>
				<br />
				<script type="text/javascript">
				$(document).ready(function() {
				    $("#rootwizard").submit(function(e) {
				        e.preventDefault();
				        $.ajax({
				            type: "POST",
				            url: "tables/backend/preliminary-data.php",
				            data: $(this).serialize(),
				            dataType: 'json',
				            success: function(data) {
				                console.log("Submit data successful!");
				            },
				            error: function(xhr, status, error) {
				                console.log("AJAX Request failed.");
				            }
				        });
				    });

					var infologin = {
						"closeButton": false,
						"debug": false,
						"positionClass": "toast-bottom-right",
						"toastClass": "black",
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
					};
					var users_name = $('#users_name').html();
					toastr.success("Now logged in as " + users_name, "Welcome!", infologin);
				});
				</script>
				<form id="rootwizard" method="post" class="form-wizard validate">
					<div class="steps-progress">
						<div class="progress-indicator"></div>
					</div>
					<!-- PROGRESS INDICATORS -->
					<ul id="datatoggles">
						<li class="active">
							<a href="#tab1" id="tab1_toggle" data-toggle="tab"><span>1</span>Interview Details</a>
						</li>
						<li>
							<a href="#tab2" id="tab2_toggle" data-toggle="tab"><span>2</span>Demographics I</a>
						</li>
						<li>
							<a href="#tab3" id="tab3_toggle" data-toggle="tab"><span>3</span>Demographics II</a>
						</li>
						<li>
							<a href="#tab4" id="tab4_toggle" data-toggle="tab"><span>4</span>Church</a>
						</li>
						<li>
							<a href="#tab5" id="tab5_toggle" data-toggle="tab"><span>5</span>Feedback</a>
						</li>
					</ul>
					<!-- END OF PROGRESS INDICATORS -->
					<div class="tab-content">
						<div class="tab-pane active" id="tab1">
							<!-- 1st Tab -->
							<?php include 'tables/tab1.php'; ?>
						</div>
						<div class="tab-pane" id="tab2">
							<!-- 2nd Tab -->
							<?php include 'tables/tab3.php'; ?>
						</div>
						<div class="tab-pane" id="tab3">
							<!-- 3rd Tab -->
							<?php include 'tables/tab4.php'; ?>
						</div>
						<div class="tab-pane" id="tab4">
							<!-- 4th Tab -->
							<?php include 'tables/tab6.php'; ?>
						</div>
						<div class="tab-pane" id="tab5">
							<!-- 5th Tab -->
							<?php include 'tables/tab7.php'; ?>
						</div>
						<ul class="pager wizard">
							<li class="previous">
								<button type="button" id="btn-prev" class="btn btn-blue btn-icon" name="next"><i class="entypo-left-open"></i>Previous</button>
							</li>
							<li class="next">
								<button type="button" id="btn-next" class="btn btn-blue btn-icon" name="previous">Next<i class="entypo-right-open"></i></button>
							</li>
						</ul>
					</div>
				</form>
			</div>
			<!-- Footer -->
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/footer.php'; ?>
		</div>
		<?php include 'modals/b1-modal.php'; ?>
		<?php include 'modals/b2-modal.php'; ?>
		<?php include 'modals/b3-modal.php'; ?>
		<?php include 'modals/c1-modal.php'; ?>
		<?php include 'modals/d1-modal.php'; ?>
		<?php include 'modals/e1-modal.php'; ?>
		<?php include 'modals/e2-modal.php'; ?>
		<?php include 'modals/e3-modal.php'; ?>
		<?php include 'modals/f1-modal.php'; ?>
		<?php include 'modals/f2-modal.php'; ?>
		<?php include 'modals/f3-modal.php'; ?>
		<?php include 'modals/accountstats-modal.php'; ?>
		<?php include 'modals/submit-modal.php'; ?>
		<?php include 'modals/reports_restrict.php'; ?>
		<?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/afterload_scripts.php'; ?>
		<script src="/extrajs.js"></script>
		<script src="/input-masks.js"></script>
		<script src="kendo-grid-tables.js"></script>
	</body>
</html>