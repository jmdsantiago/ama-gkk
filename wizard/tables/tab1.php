<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <h3><label class="control-label" for="qn_num">Questionnaire Number: </label></h3>
            <input type="text" class="form-control input-lg popover-primary" name="qn_num" id="qn_num" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the questionnaire number." data-original-title="Tip" data-validate="required" data-message-required="Please enter the questionnaire number." placeholder="Enter the questionnaire number on the survey form."/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <h3><label class="control-label" for="respondent_gkk">Select the respondent's GKK: </label></h3>
            <select id="respondent_gkk" name="respondent_gkk" class="select2" data-allow-clear="true" data-placeholder="Select a GKK">
                <optgroup label="Barangay 2-A">
                    <option value="11">Sta Cruz Chapel (Magallanes)</option>
                    <option value="12">Sto Nino Chapel (Magallanes)</option>
                </optgroup>
                <optgroup label="Barangay 5-A">
                    <option value="13">Mother of Perpetual Help Chapel (Bangkerohan Riverside)</option>
                    <option value="21">Sto Nino Chapel (Pag-asa, Bangkerohan)</option>
                    <option value="22">Mother of Perpetual Help Chapel (Bangkerohan Public Market)</option>
                    <option value="31">Sto Nino Chapel (Don Isidro Village)</option>
                    <option value="32">San Isidro Labrador Chapel (Datu Bago Street, Bangkerohan)</option>
                    <option value="33">Sto Nino Chapel (Suba, Lower Madapo)</option>
                </optgroup>
                <optgroup label="Barangay 6-A">
                    <option value="23">Immaculate Conception Chapel (San Pedro Extension)</option>
                </optgroup>
                <optgroup label="Barangay 7-A">
                    <option value="71">Nstra. Senora del Carmen Chapel (Father Selga Street)</option>
                    <option value="72">Sto Nino Chapel (Malvar Street)</option>
                    <option value="73">Our Lady of Fatima Chapel (Malvar Street)</option>
                    <option value="81">San Jose Mamumuo Chapel (Mt. Apo Street)</option>
                    <option value="82">Mother of Perpetual Help Chapel (Mt. Apo Street)</option>
                </optgroup>
                <optgroup label="Barangay 8-A">
                    <option value="34">Nstra Senora del Pilar Chapel (Lower Madapo)</option>
                    <option value="41">San Pedro Apostol Chapel (Father Selga Street)</option>
                    <option value="42">San Isidro Labrador Chapel (Father Selga Street)</option>
                    <option value="43">San Vicente Ferrer Chapel (Father Selga Street)</option>
                    <option value="51">San Roque Chapel (Madapo Hills)</option>
                    <option value="52">San Jose Mamumuo Chapel (MIH, Madapo Hills)</option>
                    <option value="53">Mother of Perpetual Help Chapel (Gatdula Village)</option>
                    <option value="62">Sagrada Familia Chapel (Father Selga Street)</option>
                    <option value="63">San Nicolas de Tolentino Chapel (Father Selga Street)</option>
                </optgroup>
                <optgroup label="Barangay 9-A">
                    <option value="61">San Rafael Chapel (Domsat Road)</option>
                    <option value="83">San Antonio de Padua Chapel (Camus Extension)</option>
                </optgroup>
            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <h3><label class="control-label" for="interviewer_name">Full Name of Interviewer</label></h3>
            <input type="text" class="form-control input-lg popover-primary" name="interviewer_name" id="interviewer_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the name of the interviewer." data-original-title="Tip" data-validate="required" data-message-required="Please enter the full name of the interviewer." placeholder="Enter the name of the interviewer."/>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <h3><label class="control-label" for="interviewer_date">Interview Date</label></h3>
            <span>Format: MM-DD-YYYY</span>
            <input type="text" name="interviewer_date" id="interviewer_date" class="form-control datepicker input-lg popover-primary" data-format="mm-dd-yyyy" data-toggle="popover" data-trigger="hover" data-placement="right" data-content="Select the date of the interview by clicking on the icon below." data-original-title="Tip" data-validate="required" data-message-required="Please enter the date of the interview." placeholder="Select a date by clicking on the icon." value="<?php echo date("m-d-Y") ?>"/>
            <div class="input-group-addon">
                <a href="#"><i class="entypo-calendar"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <h3><label class="control-label" for="interview_timestart">Start</label></h3>
            <input type="text" name="interview_timestart" id="interview_timestart" class="form-control timepicker input-lg popover-primary" data-template="dropdown" data-show-seconds="false" data- data-show-meridian="true" data-minute-step="1" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Select the time the interview started on selected date by clicking on the icon below." data-original-title="Tip" data-validate="required" data-message-required="Please enter the time the interview started."/>
            <div class="input-group-addon">
                <a href="#"><i class="entypo-clock"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <h3><label class="control-label" for="interview_timeend">End</label></h3>
            <input type="text" name="interview_timeend" id="interview_timeend" class="form-control timepicker input-lg popover-primary" data-template="dropdown" data-show-seconds="false" data-show-meridian="true" data-minute-step="1" data-toggle="popover" data-trigger="hover" data-placement="top" data-content="Select the time the interview ended on selected date by clicking on the icon below." data-original-title="Tip" data-validate="required" data-message-required="Please enter the time the interview ended."/>
            <div class="input-group-addon">
                <a href="#"><i class="entypo-clock"></i></a>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <h3><label class="control-label">Is this the <b>first</b> or <b>second</b> time you've been interviewed?</label></h3>
            <div class="radio radio-replace color-primary" >
                <input type="radio" id="rd-a0-1-interview" name="rd-a0_1">
                <label id="rd-a0-1-lbl">First</label>
            </div>
            <br>
            <div class="radio radio-replace color-primary" >
                <input type="radio" id="rd-a0-2-interview" name="rd-a0_1" checked>
                <label id="rd-a0-2-lbl">Second</label>
            </div>
        </div>
    </div>
</div>

<!-- RESPONDENTS PROFILE -->
<hr>

<h1>A. RESPONDENT'S PROFILE</h1>
<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <h3><label class="control-label" for="a1_fullname">Complete Name</label></h3>
            <input type="text" class="form-control input-lg popover-primary" name="a1_fullname" id="a1_fullname" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the full name of the respondent." data-original-title="Tip" data-validate="required" data-message-required="Please enter the full name of the respondent." placeholder="Enter the respondent's full name"/>
        </div>
    </div>
    <div class="col-md-3">
        <h3><label class="control-label">Gender</label></h3>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-a1-1-gender" name="rd-a1_1">
            <label id="rd-a1-1-lbl">Male</label>
        </div>
        <br>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-a1-2-gender" name="rd-a1_1" checked>
            <label id="rd-a1-2-lbl">Female</label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <h3><label class="control-label" for="a3_fulladdr">Complete Address</label></h3>
            <textarea class="form-control autogrow input-lg popover-primary" id="a3_fulladdr" name="a3_fulladdr" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the full address of the respondent." data-original-title="Tip" data-validate="required" data-message-required="Please enter the address of the respondent." placeholder="Enter the respondent's address"></textarea>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <h3><label class="control-label" for="a3_telnum">Telephone Number</label></h3>
            <input type="text" class="form-control input-lg popover-primary" name="a3_telnum" id="a3_telnum" data-mask="999.9999" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the respondent's telephone number." data-original-title="Tip" placeholder="Please enter the respondent's telephone number" data-validate="required" data-message-required="This field is required. Input all 0's if no telephone number is specified."/>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <h3><label class="control-label" for="a3_telnum">Cellphone Number</label></h3>
            <input type="text" class="form-control input-lg popover-primary" name="a3_celnum" id="a3_celnum" data-mask="99999999999" placeholder="Please enter the respondent's cellular number" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the respondent's cellphone number." data-original-title="Tip" data-validate="required" data-message-required="This field is required. Input all 0's if no cellphone number is specified."/>
        </div>
    </div>
<input type="hidden" id="gender" name="gender">
<input type="hidden" id="rspondnt_num" name="rspondnt_num">
<input type="hidden" name="registered_date" id="registered_date" class="form-control datepicker input-lg" data-format="mm.dd.yyyy">
<input type="hidden" id="interview_value" name="interview_value">
<input type="hidden" id="zone_num" name="zone_num">
<input type="hidden" id="survyr_num" name="survyr_num" value="<?php echo $_SESSION['usr_num']; ?>">
</div>