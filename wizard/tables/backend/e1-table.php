<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_CHURCH WHERE QN_NUMBR = 0";
		$slct_parnt = mysqli_query($conn, $statement);
		

		while($row = mysqli_fetch_array($slct_parnt, MYSQLI_ASSOC))
		{
			$data[] = $row;
		}

		
		
		echo json_encode($data);
	}


	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_church_sacrmnt_num = $request_vars["FAM_CHURCH_SACRMNT_NUM"];	
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$qn_number = $request_vars["QN_NUMBR"];	
		$chrchfamly_name = $request_vars["CHRCHFAMLY_NAME"];	
		$chrchfamly_isbaptized = $request_vars["CHRCHFAMLY_ISBAPTIZED"];	
		$chrchfamly_ksp_attndce = $request_vars["CHRCHFAMLY_KSP_ATTNDCE"];	
		$chrchfamly_confirmed = $request_vars["CHRCHFAMLY_CONFIRMED"];	
		$chrchfamly_ismarried = $request_vars["CHRCHFAMLY_ISMARRIED"];	
		$chrchfamly_marriedhow = $request_vars["CHRCHFAMLY_MARRIEDHOW"];	
		$chrchfamly_chrch_attndnce = $request_vars["CHRCHFAMLY_CHRCH_ATTNDNCE"];	
		$chrchfamly_confssn_freq = $request_vars["CHRCHFAMLY_CONFSSN_FREQ"];	
		$chrchfamly_isnotbaptized = $request_vars["CHRCHFAMLY_ISNOTBAPTIZED"];	
		$chrchfamly_reasn_nonbaptized = $request_vars["CHRCHFAMLY_REASN_NONBAPTIZED"];	
		$chrchfamly_isnotconfssn = $request_vars["CHRCHFAMLY_ISNOTCONFSSN"];	
		$chrchfamly_reasn_nonconfssn = $request_vars["CHRCHFAMLY_REASN_NONCONFSSN"];	
		$chrchfamly_isnotmarrdinchrch = $request_vars["CHRCHFAMLY_ISNOTMARRDINCHRCH"];	
		$chrchfamly_reasn_nonmarrdinchr = $request_vars["CHRCHFAMLY_REASN_NONMARRDINCHR"];

		$update = mysqli_query($conn, "UPDATE TBL_CHURCH SET 
								CHRCHFAMLY_NAME='".$chrchfamly_name."',	
								CHRCHFAMLY_ISBAPTIZED='".$chrchfamly_isbaptized."',	
								CHRCHFAMLY_KSP_ATTNDCE='".$chrchfamly_ksp_attndce."',	
								CHRCHFAMLY_CONFIRMED='".$chrchfamly_confirmed."',	
								CHRCHFAMLY_ISMARRIED='".$chrchfamly_ismarried."',	
								CHRCHFAMLY_MARRIEDHOW='".$chrchfamly_marriedhow."',	
								CHRCHFAMLY_CHRCH_ATTNDNCE='".$chrchfamly_chrch_attndnce."',	
								CHRCHFAMLY_CONFSSN_FREQ	='".$chrchfamly_confssn_freq."',
								CHRCHFAMLY_ISNOTBAPTIZED='".$chrchfamly_isnotbaptized."',	
								CHRCHFAMLY_REASN_NONBAPTIZED='".$chrchfamly_reasn_nonbaptized."',	
								CHRCHFAMLY_ISNOTCONFSSN='".$chrchfamly_isnotconfssn."',
								CHRCHFAMLY_REASN_NONCONFSSN='".$chrchfamly_reasn_nonconfssn."',	
								CHRCHFAMLY_ISNOTMARRDINCHRCH='".$chrchfamly_isnotmarrdinchrch."',	
								CHRCHFAMLY_REASN_NONMARRDINCHR='".$chrchfamly_reasn_nonmarrdinchr."'
								WHERE FAM_CHURCH_SACRMNT_NUM =".$fam_church_sacrmnt_num);
		

		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }
	
	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_church_sacrmnt_num = $request_vars["FAM_CHURCH_SACRMNT_NUM"];		
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBR"];	
		$chrchfamly_name = $request_vars["CHRCHFAMLY_NAME"];	
		$chrchfamly_isbaptized = $request_vars["CHRCHFAMLY_ISBAPTIZED"];	
		$chrchfamly_ksp_attndce = $request_vars["CHRCHFAMLY_KSP_ATTNDCE"];	
		$chrchfamly_confirmed = $request_vars["CHRCHFAMLY_CONFIRMED"];	
		$chrchfamly_ismarried = $request_vars["CHRCHFAMLY_ISMARRIED"];	
		$chrchfamly_marriedhow = $request_vars["CHRCHFAMLY_MARRIEDHOW"];	
		$chrchfamly_chrch_attndnce = $request_vars["CHRCHFAMLY_CHRCH_ATTNDNCE"];	
		$chrchfamly_confssn_freq = $request_vars["CHRCHFAMLY_CONFSSN_FREQ"];	
		$chrchfamly_isnotbaptized = $request_vars["CHRCHFAMLY_ISNOTBAPTIZED"];	
		$chrchfamly_reasn_nonbaptized = $request_vars["CHRCHFAMLY_REASN_NONBAPTIZED"];	
		$chrchfamly_isnotconfssn = $request_vars["CHRCHFAMLY_ISNOTCONFSSN"];	
		$chrchfamly_reasn_nonconfssn = $request_vars["CHRCHFAMLY_REASN_NONCONFSSN"];	
		$chrchfamly_isnotmarrdinchrch = $request_vars["CHRCHFAMLY_ISNOTMARRDINCHRCH"];	
		$chrchfamly_reasn_nonmarrdinchr = $request_vars["CHRCHFAMLY_REASN_NONMARRDINCHR"];

		$create = mysqli_query($conn, "INSERT INTO TBL_CHURCH 
								VALUES(	'".$fam_church_sacrmnt_num."',
										'".$rspondnt_num."',
										'".$survyr_num."',
										'".$qn_number."',
										'".$chrchfamly_name."',
										'".$chrchfamly_isbaptized."',
										'".$chrchfamly_ksp_attndce."',
										'".$chrchfamly_confirmed."',
										'".$chrchfamly_ismarried."',
										'".$chrchfamly_marriedhow."',
										'".$chrchfamly_chrch_attndnce."',
										'".$chrchfamly_confssn_freq."',
										'".$chrchfamly_isnotbaptized."',
										'".$chrchfamly_reasn_nonbaptized."',
										'".$chrchfamly_isnotconfssn."',
										'".$chrchfamly_reasn_nonconfssn."',
										'".$chrchfamly_isnotmarrdinchrch."',
										'".$chrchfamly_reasn_nonmarrdinchr."')"
							);
		

		 if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_church_sacrmnt_num = $request_vars["FAM_CHURCH_SACRMNT_NUM"];	
	
		$delete = mysqli_query($conn, "DELETE FROM TBL_CHURCH WHERE FAM_CHURCH_SACRMNT_NUM =".$fam_church_sacrmnt_num);
		

		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>