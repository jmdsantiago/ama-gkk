<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_CHILDREN WHERE QN_NUMBR = 0";
		$slct_parnt = mysqli_query($conn, $statement);

		while($row = mysqli_fetch_assoc($slct_parnt))
		{
			$data[] = $row;
		}
		echo json_encode($data);
	}


	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$chld_num = $request_vars["CHLD_NUM"];
		$chld_name = $request_vars["CHLD_NAME"];
		$chld_sex = $request_vars["CHLD_SEX"];
		$chld_birthdate = $request_vars["CHLD_BIRTHDATE"];
		$chld_civstatus = $request_vars["CHLD_CIVSTATUS"];
		$chld_healthstatus = $request_vars["CHLD_HEALTHSTATUS"];
		$chld_immune = $request_vars["CHLD_IMMUNE"];
		$chld_specialfeat = $request_vars["CHLD_SPECIALFEAT"];
		$chld_edu = $request_vars["CHLD_EDU"];
		$chld_isinschool = $request_vars["CHLD_ISINSCHOOL"];
		$chld_reason_no_edu = $request_vars["CHLD_REASON_NO_EDU"];

		$update = mysqli_query($conn, "UPDATE TBL_CHILDREN SET 
								CHLD_NAME='".$chld_name."',
								CHLD_SEX='".$chld_sex."',
								CHLD_BIRTHDATE='".$chld_birthdate."',
								CHLD_CIVSTATUS='".$chld_civstatus."',
								CHLD_HEALTHSTATUS='".$chld_healthstatus."',
								CHLD_IMMUNE='".$chld_immune."',
								CHLD_SPECIALFEAT='".$chld_specialfeat."',
								CHLD_EDU='".$chld_edu."',
								CHLD_ISINSCHOOL='".$chld_isinschool."',
								CHLD_REASON_NO_EDU='".$chld_reason_no_edu."'
								WHERE CHLD_NUM =".$chld_num);

		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }
	
	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$chld_num = $request_vars["CHLD_NUM"];
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBER"];
		$chld_name = $request_vars["CHLD_NAME"];
		$chld_sex = $request_vars["CHLD_SEX"];
		$chld_birthdate = $request_vars["CHLD_BIRTHDATE"];
		$chld_civstatus = $request_vars["CHLD_CIVSTATUS"];
		$chld_healthstatus = $request_vars["CHLD_HEALTHSTATUS"];
		$chld_immune = $request_vars["CHLD_IMMUNE"];
		$chld_specialfeat = $request_vars["CHLD_SPECIALFEAT"];
		$chld_edu = $request_vars["CHLD_EDU"];
		$chld_isinschool = $request_vars["CHLD_ISINSCHOOL"];
		$chld_reason_no_edu = $request_vars["CHLD_REASON_NO_EDU"];

		$create = mysqli_query($conn, "INSERT INTO TBL_CHILDREN 
								VALUES('".$chld_num."',
										'".$rspondnt_num."',
										'".$survyr_num."',
										'".$qn_number."',
										'".$chld_name."',
										'".$chld_sex."',
										'".$chld_birthdate."',
										'".$chld_civstatus."',
										'".$chld_healthstatus."',
										'".$chld_immune."',
										'".$chld_specialfeat."',
										'".$chld_edu."',
										'".$chld_isinschool."',
										'".$chld_reason_no_edu."')"
							);
		 if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$chld_num = $request_vars["CHLD_NUM"];	
	
		$delete = mysqli_query($conn, "DELETE FROM TBL_CHILDREN WHERE CHLD_NUM =".$chld_num);

		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>