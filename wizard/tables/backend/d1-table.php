<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_PARNT_SKILLS WHERE QN_NUMBR = 0";
		$slct_parnt = mysqli_query($conn, $statement);

		while($row = mysqli_fetch_array($slct_parnt, MYSQLI_ASSOC))
		{
			$data[] = $row;
		}

		
		
		echo json_encode($data);
	}


	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_skills_num = $request_vars["FAM_SKILLS_NUM"];	
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$qn_number = $request_vars["QN_NUMBR"];	
		$workfamly_name = $request_vars["WORKFAMLY_NAME"];	
		$workfamly_skills = $request_vars["WORKFAMLY_SKILLS"];	
		$workfamly_trainedhere = $request_vars["WORKFAMLY_TRAINEDHERE"];	
		$workfamly_iswillingtotrain = $request_vars["WORKFAMLY_ISWILLINGTOTRAIN"];	
		$workfamly_skillsprefer = $request_vars["WORKFAMLY_SKILLSPREFER"];

		$update = mysqli_query($conn, "UPDATE TBL_PARNT_SKILLS SET 
								WORKFAMLY_NAME='".$workfamly_name."',	
								WORKFAMLY_SKILLS='".$workfamly_skills."',	
								WORKFAMLY_TRAINEDHERE='".$workfamly_trainedhere."',	
								WORKFAMLY_ISWILLINGTOTRAIN='".$workfamly_iswillingtotrain."',	
								WORKFAMLY_SKILLSPREFER='".$workfamly_skillsprefer."'
								WHERE FAM_SKILLS_NUM =".$fam_skills_num);

		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }
	
	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_skills_num = $request_vars["FAM_SKILLS_NUM"];	
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBR"];	
		$workfamly_name = $request_vars["WORKFAMLY_NAME"];	
		$workfamly_skills = $request_vars["WORKFAMLY_SKILLS"];	
		$workfamly_trainedhere = $request_vars["WORKFAMLY_TRAINEDHERE"];	
		$workfamly_iswillingtotrain = $request_vars["WORKFAMLY_ISWILLINGTOTRAIN"];	
		$workfamly_skillsprefer = $request_vars["WORKFAMLY_SKILLSPREFER"];

		$create = mysqli_query($conn, "INSERT INTO TBL_PARNT_SKILLS 
								VALUES('".$fam_skills_num."',
										'".$rspondnt_num."',
										'".$survyr_num."',
										'".$qn_number."',
										'".$workfamly_name."',
										'".$workfamly_skills."',
										'".$workfamly_trainedhere."',
										'".$workfamly_iswillingtotrain."',
										'".$workfamly_skillsprefer."')"
							);

		 if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_skills_num = $request_vars["FAM_SKILLS_NUM"];
	
		$delete = mysqli_query($conn, "DELETE FROM TBL_PARNT_SKILLS WHERE FAM_SKILLS_NUM =".$fam_skills_num);

		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>