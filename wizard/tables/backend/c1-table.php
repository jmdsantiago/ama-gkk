<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_OFW ORDER WHERE QN_NUMBR = 0";
		$slct_parnt = mysqli_query($conn, $statement);

		while($row = mysqli_fetch_array($slct_parnt, MYSQLI_ASSOC))
		{
			$data[] = $row;
		}

		echo json_encode($data);
	}

	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$ofw_num = $request_vars["OFW_NUM"];		
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$qn_number = $request_vars["QN_NUMBR"];		
		$ofw_name = $request_vars["OFW_NAME"];	
		$ofw_sex = $request_vars["OFW_SEX"];	
		$ofw_years_ovrseas = $request_vars["OFW_YEARS_OVRSEAS"];	
		$ofw_ismemberoforg = $request_vars["OFW_ISMEMBEROFORG"];	
		$ofw_orgname = $request_vars["OFW_ORGNAME"];

		$update = mysqli_query($conn, "UPDATE TBL_OFW SET 	
								OFW_NAME= '".$ofw_name."',	
								OFW_SEX= '".$ofw_sex."',	
								OFW_YEARS_OVRSEAS= '".$ofw_years_ovrseas."',	
								OFW_ISMEMBEROFORG= '".$ofw_ismemberoforg."',	
								OFW_ORGNAME= '".$ofw_orgname."'
								WHERE OFW_NUM =".$ofw_num);

		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }
	
	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$ofw_num = $request_vars["OFW_NUM"];		
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBR"];		
		$ofw_name = $request_vars["OFW_NAME"];	
		$ofw_sex = $request_vars["OFW_SEX"];	
		$ofw_years_ovrseas = $request_vars["OFW_YEARS_OVRSEAS"];	
		$ofw_ismemberoforg = $request_vars["OFW_ISMEMBEROFORG"];	
		$ofw_orgname = $request_vars["OFW_ORGNAME"];

		$create = mysqli_query($conn, "INSERT INTO TBL_OFW
								VALUES('".$ofw_num."',
										'".$rspondnt_num."',
										'".$survyr_num."',
										'".$qn_number."',
										'".$ofw_name."',
										'".$ofw_sex."',
										'".$ofw_years_ovrseas."',
										'".$ofw_ismemberoforg."',
										'".$ofw_orgname."')"
							);

		 if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$ofw_num = $request_vars["OFW_NUM"];	
	
		$delete = mysqli_query($conn, "DELETE FROM TBL_OFW WHERE OFW_NUM =".$ofw_num);

		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>