<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_PARNT WHERE QN_NUMBR = 00000";
		$slct_parnt = mysqli_query($conn, $statement);

		while($row = mysqli_fetch_array($slct_parnt, MYSQLI_ASSOC))
		{
			$data[] = $row;
		}

		echo json_encode($data);
	}

	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$parnt_num = $request_vars["PARNT_NUM"];
		$parnt_name = $request_vars["PARNT_NAME"]; 
		$parnt_sex = $request_vars["PARNT_SEX"]; 
		$parnt_birthdate = $request_vars["PARNT_BIRTHDATE"]; 
		$parnt_civstatus = $request_vars["PARNT_CIVSTATUS"]; 
		$parnt_edu = $request_vars["PARNT_EDU"]; 
		$parnt_occupation = $request_vars["PARNT_OCCUPATION"]; 
		$parnt_monthinc = $request_vars["PARNT_MONTHINC"]; 
		$parnt_religion = $request_vars["PARNT_RELIGION"]; 
		$parnt_marriage = $request_vars["PARNT_MARRIAGE"]; 
		$parnt_numbirths = $request_vars["PARNT_NUMBIRTHS"]; 
		$parnt_numbirthsalive = $request_vars["PARNT_NUMBIRTHSALIVE"]; 
		$parnt_residenceyears = $request_vars["PARNT_RESIDENCEYEARS"]; 
		$parnt_tribe = $request_vars["PARNT_TRIBE"];

		$update = mysqli_query($conn, "UPDATE TBL_PARNT SET 
								PARNT_NAME='".$parnt_name."', 
								PARNT_SEX='".$parnt_sex."', 
								PARNT_BIRTHDATE='".$parnt_birthdate."', 
								PARNT_CIVSTATUS='".$parnt_civstatus."', 
								PARNT_EDU='".$parnt_edu."', 
								PARNT_OCCUPATION='".$parnt_occupation."', 
								PARNT_MONTHINC='".$parnt_monthinc."', 
								PARNT_RELIGION='".$parnt_religion."', 
								PARNT_MARRIAGE='".$parnt_marriage."', 
								PARNT_NUMBIRTHS='".$parnt_numbirths."', 
								PARNT_NUMBIRTHSALIVE='".$parnt_numbirthsalive."', 
								PARNT_RESIDENCEYEARS='".$parnt_residenceyears."', 
								PARNT_TRIBE='".$parnt_tribe."' 
								WHERE PARNT_NUM =".$parnt_num);

		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }
	
	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$parnt_num = $request_vars["PARNT_NUM"];
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBER"]; 
		$parnt_name = $request_vars["PARNT_NAME"]; 
		$parnt_sex = $request_vars["PARNT_SEX"]; 
		$parnt_birthdate = $request_vars["PARNT_BIRTHDATE"]; 
		$parnt_civstatus = $request_vars["PARNT_CIVSTATUS"]; 
		$parnt_edu = $request_vars["PARNT_EDU"]; 
		$parnt_occupation = $request_vars["PARNT_OCCUPATION"]; 
		$parnt_monthinc = $request_vars["PARNT_MONTHINC"]; 
		$parnt_religion = $request_vars["PARNT_RELIGION"]; 
		$parnt_marriage = $request_vars["PARNT_MARRIAGE"]; 
		$parnt_numbirths = $request_vars["PARNT_NUMBIRTHS"]; 
		$parnt_numbirthsalive = $request_vars["PARNT_NUMBIRTHSALIVE"]; 
		$parnt_residenceyears = $request_vars["PARNT_RESIDENCEYEARS"]; 
		$parnt_tribe = $request_vars["PARNT_TRIBE"];

		$create = mysqli_query($conn, "INSERT INTO TBL_PARNT 
								VALUES('".$parnt_num."',
										'".$rspondnt_num."',
										'".$qn_number."',
										'".$parnt_name."',
										'".$parnt_sex."',
										'".$parnt_birthdate."',
										'".$parnt_civstatus."',
										'".$parnt_edu."',
										'".$parnt_occupation."',
										'".$parnt_monthinc."',
										'".$parnt_religion."',
										'".$parnt_marriage."',
										'".$parnt_numbirths."',
										'".$parnt_numbirthsalive."',
										'".$parnt_residenceyears."',
										'".$parnt_tribe."',
										'".$survyr_num."')"
							);

 	if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$parnt_num = $request_vars["PARNT_NUM"];
	
		$delete = mysqli_query($conn, "DELETE FROM TBL_PARNT WHERE PARNT_NUM =".$parnt_num);

		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>