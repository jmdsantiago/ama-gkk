<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_SEMNRS WHERE QN_NUMBR = 0";
		$slct_parnt = mysqli_query($conn, $statement);


		while($row = mysqli_fetch_array($slct_parnt, MYSQLI_ASSOC))
		{
			$data[] = $row;
		}

		echo json_encode($data);
	}


	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$semnr_num = $request_vars["SEMNR_NUM"];
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBR"];
		$semnr_name = $request_vars["SEMNR_NAME"];
		$semnr_topic = $request_vars["SEMNR_TOPIC"];
		$semnr_date = $request_vars["SEMNR_DATE"];
		$semnr_conductd = $request_vars["SEMNR_CONDUCTD"];

		$update = mysqli_query($conn, "UPDATE TBL_SEMNRS SET
								SEMNR_NAME='".$semnr_name."',
								SEMNR_TOPIC='".$semnr_topic."',
								SEMNR_DATE='".$semnr_date."',
								SEMNR_CONDUCTD	='".$semnr_conductd."'
								WHERE SEMNR_NUM =".$semnr_num);


		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }

	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$semnr_num = $request_vars["SEMNR_NUM"];
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];
		$survyr_num = $request_vars["SURVYR_NUM"];
		$qn_number = $request_vars["QN_NUMBR"];
		$semnr_name = $request_vars["SEMNR_NAME"];
		$semnr_topic = $request_vars["SEMNR_TOPIC"];
		$semnr_date = $request_vars["SEMNR_DATE"];
		$semnr_conductd = $request_vars["SEMNR_CONDUCTD"];

		$create = mysqli_query($conn, "INSERT INTO TBL_SEMNRS
								VALUES( '".$semnr_num."',
										'".$rspondnt_num."',
										'".$survyr_num."',
										'".$qn_number."',
										'".$semnr_name."',
										'".$semnr_topic."',
										'".$semnr_date."',
										'".$semnr_conductd."')"
							);


		 if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$semnr_num = $request_vars["SEMNR_NUM"];

		$delete = mysqli_query($conn, "DELETE FROM TBL_SEMNRS WHERE SEMNR_NUM =".$semnr_num);


		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>