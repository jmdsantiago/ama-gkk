<?php
	$conn = mysqli_connect("localhost", "root", "", "mondesta_gkk");
	$verb = $_SERVER["REQUEST_METHOD"];
	header("Content-type: application/json");

	if($verb == "GET")
	{
		$statement = "SELECT * FROM TBL_CHURCH_PARTICPATE WHERE QN_NUMBR = 0";
		$slct_parnt = mysqli_query($conn, $statement);
		

		while($row = mysqli_fetch_array($slct_parnt, MYSQLI_ASSOC))
		{
			$data[] = $row;
		}
		
		echo json_encode($data);
	}


	if($verb == "POST")
	{
    	$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_church_particpte_num = $request_vars["FAM_CHURCH_PARTICPTE_NUM"];	
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];	
		$qn_number = $request_vars["QN_NUMBR"];	
		$chrchpart_name = $request_vars["CHRCHPART_NAME"];	
		$crchpart_evangelic = $request_vars["CHRCHPART_EVANGELIC"];	
		$crchpart_gkkorient = $request_vars["CRCHPART_GKKORIENT"];	
		$crchpart_bscbible = $request_vars["CRCHPART_BSCBIBLE"];	
		$crchpart_bsclitrgy = $request_vars["CRCHPART_BSCLITRGY"];	
		$crchpart_socteach = $request_vars["CRCHPART_SOCTEACH"];	
		$crchpart_euchlife = $request_vars["CRCHPART_EUCHLIFE"];	
		$crchpart_other = $request_vars["CRCHPART_OTHER"];	

		$update = mysqli_query($conn, "UPDATE TBL_CHURCH_PARTICPATE SET 
								CHRCHPART_NAME='".$chrchpart_name."',	
								CHRCHPART_EVANGELIC='".$crchpart_evangelic."',	
								CRCHPART_GKKORIENT='".$crchpart_gkkorient."',	
								CRCHPART_BSCBIBLE='".$crchpart_bscbible."',	
								CRCHPART_BSCLITRGY='".$crchpart_bsclitrgy."',	
								CRCHPART_SOCTEACH='".$crchpart_socteach."',	
								CRCHPART_EUCHLIFE='".$crchpart_euchlife."',	
								CRCHPART_OTHER	='".$crchpart_other."'
								WHERE FAM_CHURCH_PARTICPTE_NUM =".$fam_church_particpte_num);
		

		 if ($update) {
        echo true;
	    }
	    else {
	        header("HTTP/1.1 500 Internal Server Error");
	        echo false;
	    }
	
	}

	if($verb == "PUT")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_church_sacrmnt_num = $request_vars["FAM_CHURCH_PARTICPTE_NUM"];	
		$rspondnt_num = $request_vars["RSPONDNT_NUM"];
		$survyr_num = $request_vars["SURVYR_NUM"];	
		$qn_number = $request_vars["QN_NUMBR"];	
		$chrchpart_name = $request_vars["CHRCHPART_NAME"];	
		$crchpart_evangelic = $request_vars["CHRCHPART_EVANGELIC"];	
		$crchpart_gkkorient = $request_vars["CRCHPART_GKKORIENT"];	
		$crchpart_bscbible = $request_vars["CRCHPART_BSCBIBLE"];	
		$crchpart_bsclitrgy = $request_vars["CRCHPART_BSCLITRGY"];	
		$crchpart_socteach = $request_vars["CRCHPART_SOCTEACH"];	
		$crchpart_euchlife = $request_vars["CRCHPART_EUCHLIFE"];	
		$crchpart_other = $request_vars["CRCHPART_OTHER"];	

		$create = mysqli_query($conn, "INSERT INTO TBL_CHURCH_PARTICPATE 
								VALUES( '".$fam_church_sacrmnt_num."',
										'".$rspondnt_num."',
										'".$survyr_num."',
										'".$qn_number."', 
										'".$chrchpart_name."',
										'".$crchpart_evangelic."',
										'".$crchpart_gkkorient."',
										'".$crchpart_bscbible."',
										'".$crchpart_bsclitrgy."',
										'".$crchpart_socteach."',
										'".$crchpart_euchlife."',
										'".$crchpart_other."')"
							);
		

		 if ($create) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}

	if($verb == "DELETE")
	{
		$request_vars = Array();
    	parse_str(file_get_contents('php://input'), $request_vars );

    	$fam_church_particpte_num = $request_vars["FAM_CHURCH_PARTICPTE_NUM"];	
	
		$delete = mysqli_query($conn, "DELETE FROM TBL_CHURCH_PARTICPATE WHERE FAM_CHURCH_PARTICPTE_NUM =".$fam_church_particpte_num);
		

		 if ($delete) {
        echo true;
    }
    else {
        header("HTTP/1.1 500 Internal Server Error");
        echo false;
    }
	}
?>