<div class="row">
    <div class="col-md-12">
        <h1><strong>B.1 - Family Household</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12 table table-bordered responsive b1" id="tbl-fmlyhousehld">
        <!--
        <table class="table table-bordered responsive b1" id="tbl-fmlyhousehld">
            <thead>
                <tr>
                    <th data-field="PARNT_NAME">Name of Father/Mother</th>
                    <th data-field="PARNT_SEX">Gender</th>
                    <th data-field="PARNT_BIRTHDATE">Birthdate</th>
                    <th data-field="PARNT_CIVSTATUS">Civil Status</th>
                    <th data-field="PARNT_EDU">Highest Educational Attainment</th>
                    <th data-field="PARNT_OCCUPATION">Occupation</th>
                    <th data-field="PARNT_MONTHINC">Monthly Income</th>
                    <th data-field="PARNT_RELIGION">Religion</th>
                    <th data-field="PARNT_MARRIAGE">Marriage</th>
                    <th data-field="PARNT_NUMBIRTHS">Number of Births (Total)</th>
                    <th data-field="PARNT_NUMBIRTHSALIVE">Number of Births (Alive)</th>
                    <th data-field="PARNT_RESIDENCEYEARS">Residency (Years)</th>
                    <th data-field="PARNT_TRIBE">Tribe of Origin</th>
                </tr>
            </thead>
            <tbody>
                <tr id="1">
                    <td>TEST</td>
                </tr>
            </tbody>
        </table>
        -->
    </div>
    <br>
    <button type="button" id="btn-b1-add" class="btn btn-primary btn-lg" ><i class="entypo-plus"></i>Add Entry</button>
</div>
</br>
<div class="row">
    <div class="col-md-12">
        <h1><strong>B.2 - Children Status</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12 table table-bordered responsive b2" id="tbl-childstatus">
        <!--
        <table class="" >
            <thead>
                <tr>
                    <th rowspan="2">Name of Children</th>
                    <th rowspan="2">Gender</th>
                    <th rowspan="2">Birthdate</th>
                    <th rowspan="2">Civil Status</th>
                    <th rowspan="2">Nutrition Status</th>
                    <th rowspan="2">Immunization</th>
                    <th rowspan="2">Highest Educational Attainment</th>
                    <th colspan="2">Currently in School?</th>
                </tr>
                <tr>
                    <th>Yes</th>
                    <th>No</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
        -->
    </div>
    <button type="button" id="btn-b2-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>
</br>
<div class="row">
    <div class="col-md-12">
        <h1><strong>B.3 - House and Land Ownership</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12 table table-bordered responsive b3" id="tbl-houselndownr">
        <!--
        <table class="table table-bordered responsive" >
            <thead>
                <tr>
                    <th rowspan="2">Item</th>
                    <th rowspan="2">Owned</th>
                    <th rowspan="2">Rented</th>
                    <th rowspan="2">Mortgaged</th>
                    <th rowspan="2">Non-Formal Settlers</th>
                    <th colspan="4">House Build</th>
                    <th colspan="4">House Features</th>
                </tr>
                <tr>
                    <th>Concrete</th>
                    <th>Semi-Concrete</th>
                    <th>Wood</th>
                    <th>Other (Specify)</th>
                    <th>Bedroom</th>
                    <th>Kitchen</th>
                    <th>Toilet</th>
                    <th>Living Room</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
        -->
    </div>
    <br>
    <button type="button" id="btn-b3-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>
<br>
<hr>
<div class="row">
    <div class="col-md-12">
        <h1>LEGEND:</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <br>
        <strong>Civil Status:</strong>
        <ul class="list-group">
            <li class="list-group-item">
                <span class="badge badge-info">S</span>
                Single
            </li>
            <li class="list-group-item">
                <span class="badge badge-info">M</span>
                Married
            </li>
            <li class="list-group-item">
                <span class="badge badge-info">W</span>
                Widowed
            </li>
            <li class="list-group-item">
                <span class="badge badge-info">SP</span>
                Separated
            </li>
            <li class="list-group-item">
                <span class="badge badge-info">LSP</span>
                Legally Separated
            </li>
            <li class="list-group-item">
                <span class="badge badge-info">SGP</span>
                Single Parent
            </li>
        </ul>
    </div>
    <div class="col-md-3">
        <br>
        <strong>Monthly Income:</strong>
        <ul class="list-group">
            <li class="list-group-item">
                <span class="badge badge-secondary">0</span>
                No income
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">A</span>
                1,000 - 3,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">B</span>
                3,001 - 5,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">C</span>
                5,001 - 10,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">D</span>
                10,001 - 15,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">E</span>
                15,001 - 20,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">F</span>
                20,001 - 25,000
            </li>
        </ul>
    </div>
    <div class="col-md-3">
        <br>
        <br>
        <ul class="list-group">
            <li class="list-group-item">
                <span class="badge badge-secondary">G</span>
                25,001 - 30,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">H</span>
                30,001 - 35,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">I</span>
                35,001 - 40,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">J</span>
                40,001 - 45,000
            </li>
            <li class="list-group-item">
                <span class="badge badge-secondary">K</span>
                45,001 - <b>∞</b>
            </li>
        </ul>
    </div>
    <div class="col-md-3">
        <br>
        <strong>Religion:</strong>
        <ul class="list-group">
            <li class="list-group-item">
                <span class="badge badge-danger">RC</span>
                Roman Catholic
            </li>
            <li class="list-group-item">
                <span class="badge badge-danger">I</span>
                Islam
            </li>
            <li class="list-group-item">
                <span class="badge badge-danger">P</span>
                Protestant
            </li>
            <li class="list-group-item">
                <span class="badge badge-danger">INC</span>
                Iglesia ni Cristo
            </li>
            <li class="list-group-item">
                <span class="badge badge-danger">BA</span>
                Born-Again
            </li>
            <li class="list-group-item">
                <span class="badge badge-danger">JW</span>
                Jehovah's Witness
            </li>
            <li class="list-group-item">
                <span class="badge badge-danger">O</span>
                Others
            </li>
        </ul>
    </div>
</div>