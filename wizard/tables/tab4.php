<div class="row">
    <div class="col-md-12">
        <h1><strong>C.1 - Overseas Filipino Workers</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Are there OFW's in your family?</h3>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-ofw-1" name="rd-ofw">
            <label id="rd-ofw-1-lbl">Yes</label>
        </div>
        <br>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-ofw-2" name="rd-ofw" checked>
            <label id="rd-ofw-2-lbl">No</label>
        </div>
    </div>
</div>
<div class="row" id="tbl-ofw-container" style="display: none">
    <h4>If yes, please fill-up the following table:</h4>
    <div class="col-md-12" id="tbl-ofw">
        <!--
        <table class="table table-bordered responsive" >
            <thead>
                <tr>
                    <th>Name of Family Member</th>
                    <th>Gender</th>
                    <th>Number of Years as an OFW</th>
                    <th>Member of an OFW Organization?</th>
                    <th>If Yes, please indicate the name of the organization</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
        -->
    </div>
    <br>
    <br>
    <button type="button" id="btn-c1-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>

<div class="row">
    <div class="col-md-12">
        <h1><strong>D.1 - Family Member Livelihood Skills</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12"  id="tbl-livelihoodsklls">
    <!--
        <table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th>Name of Working Age Family Member</th>
                    <th>List of Livelihood Skills</th>
                    <th>Training Received</th>
                    <th>If no Livelihood Skills, Is he/she willing to be trained?</th>
                    <th>What skills would he/she prefer?</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
    -->
    </div>
    <br>
    <br>
        <button type="button" id="btn-d1-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>