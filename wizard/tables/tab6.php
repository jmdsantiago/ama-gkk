<div class="row">
    <div class="col-md-12">
        <h1><strong>E.1 - Church Sacraments</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12" id="tbl-churchsac">
    <!--
        <table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th rowspan="2">Family Members (List all)</th>
                    <th colspan="2">Baptized?</th>
                    <th colspan="2">Frequency of Attendance in Kasulogan sa Pulong</th>
                    <th colspan="2">Confirmation</th>
                    <th colspan="2">Married</th>
                    <th colspan="2">Frequency of Attendance of Eucharistic Celebration</th>
                    <th colspan="2">Frequency of Confession</th>
                </tr>
                <tr>
                    <th>Yes</th>
                    <th>No</th>
                    <th>Regularly</th>
                    <th>Sometimes</th>
                    <th>Yes</th>
                    <th>No</th>
                    <th>Yes</th>
                    <th>No</th>
                    <th>Every Sunday</th>
                    <th>Sometimes</th>
                    <th>Once a Year</th>
                    <th>Sometimes</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td> </td>
                </tr>
            </tbody>
        </table>
    -->

    </div>
        <br>
        <br>
        <button type="button" id="btn-e1-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>
<br>
<div class="row">
    <div class="col-md-12">
        <h1><strong>E.2</strong></h1>
        <h3>Have you attended / participated in Church sponsored seminar / training program? &nbsp;</h3>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-e2_1-1" name="rd-e2_1">
            <label id="rd-e2_1-1-lbl">Yes</label>
        </div>
        <br>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-e2_1-2" name="rd-e2_1" checked>
            <label id="rd-e2_1-2-lbl">No</label>
        </div>
    </div>
</div>
<div class="row" id="tbl-e2_1-container" style="display: none">
    <h4>If yes, please check seminar / training program participated / attended below</h4>
    <div class="col-md-12"  id="tbl-e2_1">
        <!--
        <table class="table table-bordered responsive">
            <thead>
                <tr>
                    <th rowspan="2">Name of Family member (14 years old and above only)</th>
                    <th colspan="7">Church Seminars/Training Participated</th>
                </tr>
                <tr>
                    <th>Evangelization</th>
                    <th>GKK Orientation</th>
                    <th>Basic Bible</th>
                    <th>Basic Liturgy</th>
                    <th>Social Teaching</th>
                    <th>Life in the Eucharist</th>
                    <th>Others (Please specify)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
        -->
    </div>
    <br>
    <br>
    <button type="button" id="btn-e2-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <h1><strong>E.3</strong></h1>
        <h3>Have you attended / participated in other seminar / training programs sponsored by the government or private organization? &nbsp;</h3>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-e3_1-1" name="rd-e3_1">
            <label id="rd-e3_1-1-lbl">Yes</label>
        </div>
        <br>
        <div class="radio radio-replace color-primary" >
            <input type="radio" id="rd-e3_1-2" name="rd-e3_1" checked>
            <label id="rd-e3_1-2-lbl">No</label>
        </div>
    </div>
</div>
<div class="row" id="tbl-e3_1-container" style="display: none">
    <h4>If yes, plese list the seminars / training programs you have attended.</h4>
    <div class="col-md-12" id="tbl-e3_1">
        
        <!--
        <table class="table table-bordered responsive" >
            <thead>
                <tr>
                    <th>Title of Seminar/Training Program</th>
                    <th>Subject/Topic</th>
                    <th>Date Taken</th>
                    <th>Conducted by or Main Sponsor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
        -->
    </div>
    <br>
    <br>
    <button type="button" id="btn-e3-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button>
</div>