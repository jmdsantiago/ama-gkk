<div class="row">
    <div class="col-md-12">
        <h1><strong>F. - Feedback on GKK / Parish Program Involvement</strong></h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <strong>
        Please list down what you think are the priority problems of the GKK / Community where you live at present. &nbsp;
        </strong>
        <br>
        <div id="tbl-f1" class="form-group">

        </div>
        <button type="button" id="btn-f1-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button> &nbsp;
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <strong>
        Please list down what you think are the solutions to these priority problems of the GKK / Community where you live at present. &nbsp;
        </strong>
        <br>
        <div id="tbl-f2" class="form-group">
        </div>
        <button type="button" id="btn-f2-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button> &nbsp;
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <strong>
        Please list down what you can contribute to solve these priority problems where you live at present. &nbsp;
        </strong>
        <br>
        <div id="tbl-f3" class="form-group">
        </div>
        <button type="button" id="btn-f3-add" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Add Entry</button> &nbsp;
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-12">
        <button type="button" id="btn-confirm" class="btn btn-primary btn-lg"><i class="entypo-plus"></i>Submit Data</button>
    </div>
</div>