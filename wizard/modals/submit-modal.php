<div class="modal" id="modal-submitall" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Notification</h2>
            </div>
            <div class="modal-body">
                <div class="col-md-12 center-block">
                    <p>Ready to submit all data to the database?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Not Yet</button>
                    <button type="submit" id="btn_submit_all" class="btn btn-primary btn-lg">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal animated bounceIn" id="modal-confirmclose" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Data Submitted</h2>
            </div>
            <div class="modal-body">
                <div class="col-md-12 center-block">
                    <p class="text-center">Would you like to create another entry?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btn_confirm_close" class="btn btn-default btn-lg">No</button>
                    <button type="button" id="btn_confirm_new" class="btn btn-primary btn-lg">Yes</button>
                </div>
            </div>
        </div>
    </div>
</div>

