<form id="b3form">
    <div class="modal custom-width" id="modal-b3">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">House / Land Ownership</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b3_item">Item</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b3_item" id="tbl-b3_item" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input what type of item of land or home the respondent has (E.g. house, residential lot)." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Owned</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-owned" name="tbl-b3_owned">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-owned" name="tbl-b3_owned" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Rented</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-rented" name="tbl-b3_rented">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-rented" name="tbl-b3_rented" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Mortgaged</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-mortgage" name="tbl-b3_mortgage">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-mortgage" name="tbl-b3_mortgage" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b3_nonformal">Non-Formal Settlers</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-settler" name="tbl-b3_settler">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-settler" name="tbl-b3_settler" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">House Build</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-build-concrete" name="tbl-b3_housebuild">
                                    <label id="rd-b3-lbl-build-concrete">Concrete</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-build-semiconcrete" name="tbl-b3_housebuild" checked="true">
                                    <label id="rd-b3-lbl-build-semiconcrete">Semi-concrete</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-build-wood" name="tbl-b3_housebuild">
                                    <label id="rd-b3-lbl-build-wood">Wood</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-build-other" name="tbl-b3_housebuild">
                                    <label id="rd-b3-lbl-build-other">Other (Specify)</label>
                                </div>
                                <div class="form-group" id="b3_other_container">
                                    <input type="text" class="form-control input-lg popover-primary" name="tbl-b3_other" id="tbl-b3_other" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="If the item has a different kind of make or build, specify it here if applicable." data-original-title="Tip"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Bedroom</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-bedrm" name="tbl-b3_bedrm">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-bedrm" name="tbl-b3_bedrm" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Kitchen</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-kitchn" name="tbl-b3_kitchn">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-kitchn" name="tbl-b3_kitchn" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Toilet</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-toilet" name="tbl-b3_toilet">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-toilet" name="tbl-b3_toilet" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Living Room</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-1-lvngrm" name="tbl-b3_lvngrm">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b3-2-lvngrm" name="tbl-b3_lvngrm" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="tbl-b3-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>