<form id="e3form">
    <div class="modal custom-width" id="modal-e3" data-backdrop="static">
        <div class="modal-dialog" style="width: 60%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">E.3</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-e3_title">Title of Seminar / Training Program</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-e3_title" id="tbl-e3_title" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the title of the seminar or training program this person has attended." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-e3_topic">Subject / Topic</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-e3_topic" id="tbl-e3_topic" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="What subject or topic did this seminar or training program cover?." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h3><label class="control-label" for="tbl-e3_date">When Conducted</label></h3>
                            <input type="text" name="tbl-e3_date" id="tbl-e3_date" class="form-control input-lg popover-primary" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the date or year or month this seminar or training program was taken." data-original-title="Tip">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <h3><label class="control-label" for="tbl-e3_conduct">Conducted by / Main Sponsor</label></h3>
                            <input type="text" class="form-control input-lg popover-primary" name="tbl-e3_conduct" id="tbl-e3_conduct" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the name of the company or person that sponsored or conducted this seminar or training program." data-original-title="Tip"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="tbl-e3-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>