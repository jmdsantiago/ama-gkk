<form id="e1form">
    <div class="modal custom-width" id="modal-e1" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">E.1</h4>
                </div>
                <div class="modal-body">
                <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-e1_name">Name of Family Member</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-e1_name" id="tbl-e1_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the full name of this family member." data-original-title="Tip" data-validate="required" data-message-required="Please enter the name of this family member."/>
                            </div>
                        </div>
                </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Baptized</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-1-baptism" name="tbl-e1_baptism" checked>
                                    <label id="rd-e1-1-lbl-baptism">Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-2-baptism" name="tbl-e1_baptism">
                                    <label id="rd-e1-2-lbl-baptism">No</label>
                                </div>
                            </div>
                            <div class="form-group" id="tbl-e1_1-container" style="display: none">
                                <h4>If no, Why?</h4>
                                <input type="text" class="form-control input-lg" name="e1_1-why" id="e1_1-why" placeholder="Please state the reason" data-validate="required" data-message-required="Please state a reason why."/>
                            </div>
                        </div>
                      <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Confirmation</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-1-confirm" name="tbl-e1_confirm" checked>
                                    <label id="rd-e1-1-lbl-confirm">Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-2-confirm" name="tbl-e1_confirm">
                                    <label id="rd-e1-2-lbl-confirm">No</label>
                                </div>
                            </div>
                        <div class="form-group" id="tbl-e1_2-container" style="display: none">
                                <h4>If no, Why?</h4>
                                <input type="text" class="form-control input-lg" name="e1_2-why" id="e1_2-why" placeholder="Please state the reason" data-validate="required" data-message-required="Please state a reason why."/>
                        </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Marriage</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-1-marriage" name="tbl-e1_marriage" checked>
                                    <label id="rd-e1-1-lbl">Civil</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-2-marriage" name="tbl-e1_marriage" checked>
                                    <label id="rd-e1-2-lbl">Church</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-3-marriage" name="tbl-e1_marriage">
                                    <label id="rd-e1-3-lbl">No</label>
                                </div>
                            </div>
                            <div class="form-group" id="tbl-e1_3-container" style="display: none">
                                <h4>If not married in Church. Why?</h4>
                                <input type="text" class="form-control input-lg" name="e1_3-why" id="e1_3-why" placeholder="Please state the reason" data-validate="required" data-message-required="Please state a reason why."/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4><label class="control-label" for="tbl-e1_ksp">Kasaulogan sa Pulong (Attendance Frequency)</label></h4>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-1-ksp" name="tbl-e1_ksp" checked>
                                    <label>Regularly</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-2-ksp" name="tbl-e1_ksp" checked>
                                    <label>Sometimes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-3-ksp" name="tbl-e1_ksp" checked>
                                    <label>None</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h4><label class="control-label" for="tbl-e1_chrchattndnce">Church (Attendance Frequency)</label></h4>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-1-crchattndnce" name="tbl-e1_crchattndnce" checked>
                                    <label>Every Sunday</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-2-crchattndnce" name="tbl-e1_crchattndnce" checked>
                                    <label>Sometimes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-3-crchattndnce" name="tbl-e1_crchattndnce" checked>
                                    <label>None</label>
                                </div>
                            </div>
                        </div>

                    <div class="col-md-4">
                            <div class="form-group">
                                <h4><label class="control-label" for="tbl-e1_confession">Confession (Frequency)</label></h4>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-1-confession" name="tbl-e1_confession" checked>
                                    <label>Once a year</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-2-confession" name="tbl-e1_confession" checked>
                                    <label>Sometimes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e1-3-confession" name="tbl-e1_confession" checked>
                                    <label>None</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" id="tbl-e1-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>