<form id="f3form">
    <div class="modal custom-width" id="modal-f3" data-backdrop="static">
        <div class="modal-dialog" style="width: 50%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">F.3</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-f3_feedback">Feedback</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-f3_feedback" id="tbl-f3_feedback" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the persons feedback." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="tbl-f3-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>