<form id="b1form">
    <div class="modal custom-width" id="modal-b1" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Family Household Data</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_name">Name of Father/Mother</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_name" id="tbl-b1_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the full name of the respondent's mother or father." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_edu">Highest Educational Attainment</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_edu" id="tbl-b1_edu" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the highest education level that this parent attained." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1-bday">Birthdate</label></h3>
                                <span>Format: MM-DD-YYYY</span>
                                <input type="text" name="tbl-b1-bday" id="tbl-b1-bday" class="form-control datepicker input-lg popover-primary" data-format="mm-dd-yyyy" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the birthdate of this parent." data-original-title="Tip">
                                <div class="input-group-addon">
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Sex</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b1-1-gender" name="tbl-b1_sex">
                                    <label id="rd-b1-1-lbl">Male</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b1-2-gender" name="tbl-b1_sex" checked>
                                    <label id="rd-b1-2-lbl">Female</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Marriage</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b1-1-marriage" name="tbl-b1_marriage">
                                    <label id="rd-b1-1-lbl">Civil</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b1-2-marriage" name="tbl-b1_marriage" checked>
                                    <label id="rd-b1-2-lbl">Church</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b1-3-marriage" name="tbl-b1_marriage" checked>
                                    <label id="rd-b1-3-lbl">Civil &amp; Church</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b1-4-marriage" name="tbl-b1_marriage" checked>
                                    <label id="rd-b1-4-lbl">None</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_occu">Occupation</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_occu" id="tbl-b1_occu" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the current job of this parent." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_civstat">Civil Status</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_civstat" id="tbl-b1_civstat" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Select the corresponding letter which best matches the civil status of this person from the table below." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_income">Monthly Income</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_income" id="tbl-b1_income" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Select the corresponding letter which best matches the monthly income of this person from the table below." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_religion">Religion</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_religion" id="tbl-b1_religion" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Select the corresponding letter which best matches the religion of this person from the table below." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_numbirthtotal">Number of Births (Total)</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_numbirthtotal" id="tbl-b1_numbirthtotal" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the total number of children (Alive or Dead) of this parent." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_numbirthalive">Number of Births (Alive)</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_numbirthalive" id="tbl-b1_numbirthalive" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the total number of children alive of this parent." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_residency">Residency (In years)</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_residency" id="tbl-b1_residency" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input how many years this parent is living in his/her residence." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b1_tribe">Tribe of Origin</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b1_tribe" id="tbl-b1_tribe" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the tribe from which this parent has originated." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h2>LEGEND:</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <br>
                            <strong>Civil Status:</strong>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge badge-info">S</span>
                                    Single
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-info">M</span>
                                    Married
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-info">W</span>
                                    Widowed
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-info">SP</span>
                                    Separated
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-info">LSP</span>
                                    Legally Separated
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-info">SGP</span>
                                    Single Parent
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <strong>Monthly Income:</strong>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">0</span>
                                    No income
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">A</span>
                                    1,000 - 3,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">B</span>
                                    3,001 - 5,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">C</span>
                                    5,001 - 10,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">D</span>
                                    10,001 - 15,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">E</span>
                                    15,001 - 20,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">F</span>
                                    20,001 - 25,000
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <br>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">G</span>
                                    25,001 - 30,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">H</span>
                                    30,001 - 35,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">I</span>
                                    35,001 - 40,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">J</span>
                                    40,001 - 45,000
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-secondary">K</span>
                                    45,001 - infinity
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <br>
                            <strong>Religion:</strong>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <span class="badge badge-danger">RC</span>
                                    Roman Catholic
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-danger">I</span>
                                    Islam
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-danger">P</span>
                                    Protestant
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-danger">INC</span>
                                    Iglesia ni Cristo
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-danger">BA</span>
                                    Born-Again
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-danger">JW</span>
                                    Jehovah's Witness
                                </li>
                                <li class="list-group-item">
                                    <span class="badge badge-danger">O</span>
                                    Others
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-info" id="tbl-b1-save">Add Entry</button>
                </div>
            </div>
        </div>
    </div>
</form>