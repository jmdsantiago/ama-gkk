<form id="d1form">
    <div class="modal custom-width" id="modal-d1" data-backdrop="static">
        <div class="modal-dialog" style="width: 70%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Livelihood &amp; Skills</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-d1_name">Name of Working Age Family Member</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-d1_name" id="tbl-d1_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the name of the family member who is already employed." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-d1_skills">List of Livelihood Skills</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-d1_skills" id="tbl-d1_skills" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the skills of this person if applicable." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4" id="d1_training_container" style="display: none">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-d1_training">Training Received</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-d1_training" id="tbl-d1_training" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input where this person has received training for their livelihood skills." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="d1_2betrained_container">
                        <div class="col-md-4">
                            <h3>If no livelihood skills:</h3>
                            <div class="form-group">
                                <h3><label class="control-label">Is he/she willing to be trained?</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-d1-1-2betrained" name="tbl-d1_2betrained">
                                    <label id="rd-2betrained-1-lbl">Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-d1-2-2betrained" name="tbl-d1_2betrained">
                                    <label id="rd-2betrained-2-lbl">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" id="d1_preferskills_container" style="display: none">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-d1_preferskills">If yes, what skills would he/she prefer?</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-d1_preferskills" id="tbl-d1_preferskills" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the skills that this person would prefer learning." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="tbl-d1-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>