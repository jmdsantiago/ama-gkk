<form id="c1form">
    <div class="modal custom-width" id="modal-c1" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">OFW Data</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-c1_name">Name of Family Member</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-c1_name" id="tbl-c1_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the name of the family member with OFW status." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Gender</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-c1-1-gender" name="tbl-c1_gender">
                                    <label>Male</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-c1-2-gender" name="tbl-c1_gender" checked>
                                    <label>Female</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-c1_ofwyears">Number of Years as an OFW</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-c1_ofwyears" id="tbl-c1_ofwyears" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Years this family member has experience as an OFW." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Is member of an OFW organization</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-c1-1-OFWOrgMember" name="tbl-c1_OFWOrgMember">
                                    <label id="rd-OFWOrgMember-1-lbl">Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-c1-2-OFWOrgMember" name="tbl-c1_OFWOrgMember" checked>
                                    <label id="rd-OFWOrgMember-2-lbl">No</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="c1_IsOFWOrgMember_container" style="display: none">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-c1_IsOFWOrgMember">If yes, please indicate the name of the organization</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-c1_IsOFWOrgMember" id="tbl-c1_IsOFWOrgMember" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the name of the organization of the OFW family member." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="tbl-c1-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>