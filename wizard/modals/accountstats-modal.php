<form id="account_stats_form" method="post">
<div class="modal custom-width" id="modal-accountstats" data-backdrop="static">
    <div class="modal-dialog" style="width: 60%">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Your Account</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <p>Name: <b><?php echo $_SESSION['usersname']; ?></b></p>
                    </div>
                    <br>
                    <div class="col-md-4">
                        <p>Section: <b><?php echo $_SESSION['organization']; ?></b></p>
                    </div>
                    <div class="col-md-4">
                        <p>USN: <b><?php echo $_SESSION['usrname']; ?></b></p>
                    </div>
                    <div class="col-md-4">
                        <p>User ID: <b><?php echo $_SESSION['usr_num']; ?></b></p>
                        <input type="hidden" id="survyr_num" name="survyr_num" value="<?php echo $_SESSION['usr_num']; ?>">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <div class="tile-stats tile-aqua">
                        <div class="icon"><i class="entypo-newspaper"></i></div>
                        <h3>You've finished: </h3>
                        <div id="num_records_account" class="num" data-start="0" data-end="<?php echo $_SESSION['account_stats']; ?>" data-postfix=" forms" data-duration="1200" data-delay="0">0</div>
                        <h3>Wow, good job.</h3>
                        <p>(Sarcasm)</p>
                    </div>
                    </div>
                    <div class="col-md-6">
                        <img src="/assets/images/clap.gif">
                    </div>
                </div>
                <br>
                    <div class="row">
                        <div class="col-md-12">
                            <em>Warning: To see recent data, you need to refresh the page. I recommend viewing this window again after finishing a form.</em>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
</form>

