<form id="e2form">
    <div class="modal custom-width" id="modal-e2" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">E.2</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-e2_name">Name of Family Member</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-e2_name" id="tbl-e2_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the full name of this family member." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Evangelization</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-1-evangelization" name="tbl-e2_evangelization">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-2-evangelization" name="tbl-e2_evangelization" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">GKK Orientation</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-1-gkk" name="tbl-e2_gkk">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-2-gkk" name="tbl-e2_gkk" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Basic Bible</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-1-bible" name="tbl-e2_bible">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-2-bible" name="tbl-e2_bible" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Basic Liturgy</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-1-liturgy" name="tbl-e2_liturgy">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-2-liturgy" name="tbl-e2_liturgy" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Social Teaching</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-1-social" name="tbl-e2_social">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-2-social" name="tbl-e2_social" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Life in the Eucharist</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-1-eucharist" name="tbl-e2_eucharist">
                                    <label>Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-e2-2-eucharist" name="tbl-e2_eucharist" checked>
                                    <label>No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h3><label class="control-label">Others (Please specify)</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-e2_others" id="tbl-e2_others" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input any other Church sponsored seminars this person has attended." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" id="tbl-e2-save" class="btn btn-info">Add Entry</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>