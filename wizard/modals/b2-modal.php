<form id="b2form">
    <div class="modal custom-width" id="modal-b2" data-backdrop="static">
        <div class="modal-dialog" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Status of Children</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2_name">Name of Children</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2_name" id="tbl-b2_name" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the full name of this child." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2-bday">Birthdate</label></h3>
                                <span>Format: MM-DD-YYYY</span>
                                <input type="text" name="tbl-b2-bday" id="tbl-b2-bday" class="form-control datepicker input-lg popover-primary" data-format="mm-dd-yyyy" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the birthdate of this child." data-original-title="Tip">
                                <div class="input-group-addon">
                                    <a href="#"><i class="entypo-calendar"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2_civstat">Civil Status</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2_civstat" id="tbl-b2_civstat" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Select the corresponding letter which best matches the civil status of this person from the table below." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2_nutri">Nutrition Status</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2_nutri" id="tbl-b2_nutri" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the health status of this child." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2_immu">Immunization</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2_immu" id="tbl-b2_immu" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input what immunizations this child has acquired." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2-edu">Highest Educational Attainment</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2_edu" id="tbl-b2_edu" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Input the highest level of education this child has attained." data-original-title="Tip"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2-features">Special Features (PWD)</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2-features" id="tbl-b2-features" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="List one of the special features of this child." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Still in school?</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b2-1-school" name="tbl-b2_school">
                                    <label id="rd-b2-1-1-lbl">Yes</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b2-2-school" name="tbl-b2_school" checked>
                                    <label id="rd-b2-1-2-lbl">No</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <h3><label class="control-label">Sex</label></h3>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b2-1-gender" name="tbl-b2_sex">
                                    <label id="rd-b2-2-1-lbl">Male</label>
                                </div>
                                <br>
                                <div class="radio radio-replace color-primary" >
                                    <input type="radio" id="rd-b2-2-gender" name="tbl-b2_sex" checked>
                                    <label id="rd-b2-2-2-lbl">Female</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="b2_IsInSchool_container" style="display: none">
                        <div class="col-md-12">
                            <div class="form-group">
                                <h3><label class="control-label" for="tbl-b2_IsInSchool">If not in school, please state the reason why.</label></h3>
                                <input type="text" class="form-control input-lg popover-primary" name="tbl-b2_IsInSchool" id="tbl-b2_IsInSchool" data-toggle="popover" data-trigger="focus" data-placement="top" data-content="Please state the reason." data-original-title="Tip"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="tbl-b2-save" class="btn btn-info">Add Entry</button>
                </div>
            </div>
        </div>
    </div>
</form>