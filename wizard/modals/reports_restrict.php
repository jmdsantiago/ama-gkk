<div class="modal" id="reports_access_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h1 class="modal-title">Restricted Area</h1>
			</div>

			<div class="modal-body">
				<p>Sorry. Your user type is not permitted to access this page.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-lg" data-dismiss="modal">Got it</button>
			</div>
		</div>
	</div>
</div>