$(document).ready(function() {
    //B.1 - Family Household Data
    var fmlyhousehld_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/b1-table.php",
            create: {
                url: "tables/backend/b1-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING B1: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING B1: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/b1-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING B1: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING B1: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/b1-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING B1: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING B1: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    PARNT_NUM: {
                        editable: false,
                        nullable: false
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBER: {
                        editable: false,
                        nullable: false
                    },
                    PARNT_NAME: {
                        type: "string",
                        editable: false
                    },
                    PARNT_SEX: {
                        type: "string",
                        editable: false
                    },
                    PARNT_BIRTHDATE: {
                        type: "string",
                        editable: false
                    },
                    PARNT_CIVSTATUS: {
                        type: "string",
                        editable: false
                    },
                    PARNT_EDU: {
                        type: "string",
                        editable: false
                    },
                    PARNT_OCCUPATION: {
                        type: "string",
                        editable: false
                    },
                    PARNT_MONTHINC: {
                        type: "string",
                        editable: false
                    },
                    PARNT_RELIGION: {
                        type: "string",
                        editable: false
                    },
                    PARNT_MARRIAGE: {
                        type: "string",
                        editable: false
                    },
                    PARNT_NUMBIRTHS: {
                        type: "string",
                        editable: false
                    },
                    PARNT_NUMBIRTHSALIVE: {
                        type: "string",
                        editable: false
                    },
                    PARNT_RESIDENCEYEARS: {
                        type: "string",
                        editable: false
                    },
                    PARNT_TRIBE: {
                        type: "string",
                        editable: false
                    },
                },
            },
        },
        sync: function(e) {
            console.log("DEBUGGING (B1): Synchornization complete!");
        }
    });
    $("#tbl-fmlyhousehld").kendoGrid({
        dataSource: fmlyhousehld_ds,
        height: 300,
        editable: true,
        selectable: true,
        scrollable: true,
        columns: [{
            field: "PARNT_NAME",
            title: "Name of Father/Mother",
            width: "250px"
        }, {
            field: "PARNT_SEX",
            title: "Gender",
            width: "250px"
        }, {
            field: "PARNT_BIRTHDATE",
            title: "Birthdate",
            width: "250px"
        }, {
            field: "PARNT_CIVSTATUS",
            title: "Civil Status",
            width: "250px"
        }, {
            field: "PARNT_EDU",
            title: "Highest Educational Attainment",
            width: "250px"
        }, {
            field: "PARNT_OCCUPATION",
            title: "Occupation",
            width: "250px"
        }, {
            field: "PARNT_MONTHINC",
            title: "Monthly Income",
            width: "250px"
        }, {
            field: "PARNT_RELIGION",
            title: "Religion",
            width: "250px"
        }, {
            field: "PARNT_MARRIAGE",
            title: "Marriage",
            width: "250px"
        }, {
            field: "PARNT_NUMBIRTHS",
            title: "Number of Births (Total)",
            width: "250px"
        }, {
            field: "PARNT_NUMBIRTHSALIVE",
            title: "Number of Births (Alive)",
            width: "250px"
        }, {
            field: "PARNT_RESIDENCEYEARS",
            title: "Residency (Years)",
            width: "250px"
        }, {
            field: "PARNT_TRIBE",
            title: "Tribe of Origin",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });
    //B.2 - Status of Children
    var childstatus_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/b2-table.php",
            create: {
                url: "tables/backend/b2-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING b2: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING b2: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/b2-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING b2: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING b2: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/b2-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING b2: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING b2: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    CHLD_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    QN_NUMBER: {
                        editable: false,
                        nullable: true
                    },
                    CHLD_NAME: {
                        type: "string",
                        editable: false
                    },
                    CHLD_SEX: {
                        type: "string",
                        editable: false
                    },
                    CHLD_BIRTHDATE: {
                        type: "string",
                        editable: false
                    },
                    CHLD_CIVSTATUS: {
                        type: "string",
                        editable: false
                    },
                    CHLD_HEALTHSTATUS: {
                        type: "string",
                        editable: false
                    },
                    CHLD_IMMUNE: {
                        type: "string",
                        editable: false
                    },
                    CHLD_SPECIALFEAT: {
                        type: "string",
                        editable: false
                    },
                    CHLD_EDU: {
                        type: "string",
                        editable: false
                    },
                    CHLD_ISINSCHOOL: {
                        type: "string",
                        editable: false
                    },
                    CHLD_REASON_NO_EDU: {
                        type: "string",
                        editable: false
                    },
                },
            },
        },
        sync: function(e) {
                 console.log("DEBUGGING (B2): Synchornization complete!");
        }
    });
    $("#tbl-childstatus").kendoGrid({
        dataSource: childstatus_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            field: "CHLD_NAME",
            title: "Name of Children",
            width: "250px"
        }, {
            field: "RSPONDNT_NUM",
            title: "RSPONDNT_NUM",
            hidden: true
        }, {
            field: "QN_NUMBER",
            title: "QN_NUMBER",
            hidden: true
        }, {
            field: "CHLD_SEX",
            title: "Gender",
            width: "250px"
        }, {
            field: "CHLD_BIRTHDATE",
            title: "Birthdate",
            width: "250px"
        }, {
            field: "CHLD_CIVSTATUS",
            title: "Civil Status",
            width: "250px"
        }, {
            field: "CHLD_HEALTHSTATUS",
            title: "Nutrition Status",
            width: "250px"
        }, {
            field: "CHLD_IMMUNE",
            title: "Immunization",
            width: "250px"
        }, {
            field: "CHLD_SPECIALFEAT",
            title: "Special Features (PWD)",
            width: "250px"
        }, {
            field: "CHLD_EDU",
            title: "Highest Educational Attainment",
            width: "250px"
        }, {
            field: "CHLD_ISINSCHOOL",
            title: "Currently in School?",
            width: "250px"
        }, {
            field: "CHLD_REASON_NO_EDU",
            title: "Reason child is still in school",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });
    //B.3 - House and Land Ownership
    var houselndownr_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/b3-table.php",
            create: {
                url: "tables/backend/b3-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING b3: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING b3: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/b3-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING b3: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING b3: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/b3-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING b3: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING b3: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    OWNR_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    OWNRSHP_ITEM: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_OWNED: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_RENTED: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_MORTGAGE: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_INFRMAL_SETTLR: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_HOUSEBUILD: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_HASKITCHEN: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_HASBEDRUM: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_HASTOILET: {
                        type: "string",
                        editable: false
                    },
                    OWNRSHP_HASLIVNGRUM: {
                        type: "string",
                        editable: false
                    },
                }
            }
        },
        sync: function(e) {
             console.log("DEBUGGING (B3): Synchornization complete!");
        }
    });
    $("#tbl-houselndownr").kendoGrid({
        dataSource: houselndownr_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Item",
            field: "OWNRSHP_ITEM",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            title: "Owned",
            field: "OWNRSHP_OWNED",
            width: "250px"
        }, {
            title: "Rented",
            field: "OWNRSHP_RENTED",
            width: "250px"
        }, {
            title: "Mortgaged",
            field: "OWNRSHP_MORTGAGE",
            width: "250px"
        }, {
            title: "Non-Formal Settlers",
            field: "OWNRSHP_INFRMAL_SETTLR",
            width: "250px"
        }, {
            title: "House Build",
            field: "OWNRSHP_HOUSEBUILD",
            width: "250px"
        }, {
            title: "Kitchen (Features)",
            field: "OWNRSHP_HASKITCHEN",
            width: "250px"
        }, {
            title: "Bedroom (Features)",
            field: "OWNRSHP_HASBEDRUM",
            width: "250px"
        }, {
            title: "Toilet (Features)",
            field: "OWNRSHP_HASTOILET",
            width: "250px"
        }, {
            title: "Living Room (Features)",
            field: "OWNRSHP_HASLIVNGRUM",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });
    //C.1 - Overseas Filipino Workers
    var ofw_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/c1-table.php",
            create: {
                url: "tables/backend/c1-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING c1: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING c1: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/c1-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING c1: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING c1: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/c1-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING c1: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING c1: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    OFW_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    OFW_NAME: {
                        type: "string",
                        editable: false
                    },
                    OFW_SEX: {
                        type: "string",
                        editable: false
                    },
                    OFW_YEARS_OVRSEAS: {
                        type: "string",
                        editable: false
                    },
                    OFW_ISMEMBEROFORG: {
                        type: "string",
                        editable: false
                    },
                    OFW_ORGNAME: {
                        type: "string",
                        editable: false
                    },
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (C1): Synchornization complete!");
            }
    });
    $("#tbl-ofw").kendoGrid({
        dataSource: ofw_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Name of Family Member",
            field: "OFW_NAME",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            title: "Gender",
            field: "OFW_SEX",
            width: "250px"
        }, {
            title: "Number of Years as an OFW",
            field: "OFW_YEARS_OVRSEAS",
            width: "250px"
        }, {
            title: "Member of an OFW Organization?",
            field: "OFW_ISMEMBEROFORG",
            width: "250px"
        }, {
            title: "OFW Organization",
            field: "OFW_ORGNAME",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });
    //D.1 - Family Members’ Livelihood Skills
    var livelihoodsklls_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/d1-table.php",
            create: {
                url: "tables/backend/d1-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING d1: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING d1: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/d1-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING d1: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING d1: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/d1-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING d1: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING d1: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    FAM_SKILLS_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBER: {
                        editable: false,
                        nullable: true
                    },
                    WORKFAMLY_NAME: {
                        type: "string",
                        editable: false
                    },
                    WORKFAMLY_SKILLS: {
                        type: "string",
                        editable: false
                    },
                    WORKFAMLY_TRAINEDHERE: {
                        type: "string",
                        editable: false
                    },
                    WORKFAMLY_ISWILLINGTOTRAIN: {
                        type: "string",
                        editable: false
                    },
                    WORKFAMLY_SKILLSPREFER: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (D1): Synchornization complete!");
            }
    });
    $("#tbl-livelihoodsklls").kendoGrid({
        dataSource: livelihoodsklls_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Name of Working Age Family Member",
            field: "WORKFAMLY_NAME",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            title: "List of Livelihood Skills",
            field: "WORKFAMLY_SKILLS",
            width: "250px"
        }, {
            title: "Training Received",
            field: "WORKFAMLY_TRAINEDHERE",
            width: "250px"
        }, {
            title: "If no skills, is willing to be trained?",
            field: "WORKFAMLY_ISWILLINGTOTRAIN",
            width: "250px"
        }, {
            title: "Preferred Skills",
            field: "WORKFAMLY_SKILLSPREFER",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });
    //E.1 - Church Sacraments
    var churchsac_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/e1-table.php",
            create: {
                url: "tables/backend/e1-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING e1: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e1: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/e1-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING e1: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e1: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/e1-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING e1: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e1: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    FAM_CHURCH_SACRMNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    CHRCHFAMLY_NAME: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_ISBAPTIZED: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_KSP_ATTNDCE: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_CONFIRMED: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_ISMARRIED: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_MARRIEDHOW: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_CHRCH_ATTNDNCE: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_CONFSSN_FREQ: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_ISNOTBAPTIZED: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_REASN_NONBAPTIZED: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_ISNOTCONFSSN: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_REASN_NONCONFSSN: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_ISNOTMARRDINCHRCH: {
                        type: "string",
                        editable: false
                    },
                    CHRCHFAMLY_REASN_NONMARRDINCHR: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (E1): Synchornization complete!");
            }
    });
    $("#tbl-churchsac").kendoGrid({
        dataSource: churchsac_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Family Members",
            field: "CHRCHFAMLY_NAME",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            title: "Baptism",
            field: "CHRCHFAMLY_ISBAPTIZED",
            width: "250px"
        }, {
            title: "Frequency of Attendance in Kasulogan sa Pulong",
            field: "CHRCHFAMLY_KSP_ATTNDCE",
            width: "300px"
        }, {
            title: "Confirmation",
            field: "CHRCHFAMLY_CONFIRMED",
            width: "250px"
        }, {
            title: "Married",
            field: "CHRCHFAMLY_ISMARRIED",
            width: "250px"
        }, {
            title: "Marriage Process",
            field: "CHRCHFAMLY_MARRIEDHOW",
            width: "250px"
        }, {
            title: "Frequency of Attendance of Eucharistic Celebration",
            field: "CHRCHFAMLY_CHRCH_ATTNDNCE",
            width: "300px"
        }, {
            title: "Frequency of Confession",
            field: "CHRCHFAMLY_CONFSSN_FREQ",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });

    //E.2 - Church Participation
    var churchpart_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/e2-table.php",
            create: {
                url: "tables/backend/e2-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING e2: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e2: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/e2-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING e2: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e2: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/e2-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING e2: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e2: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    FAM_CHURCH_PARTICPTE_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    CHRCHPART_NAME: {
                        type: "string",
                        editable: false
                    },
                    CHRCHPART_EVANGELIC: {
                        type: "string",
                        editable: false
                    },
                    CRCHPART_GKKORIENT: {
                        type: "string",
                        editable: false
                    },
                    CRCHPART_BSCBIBLE: {
                        type: "string",
                        editable: false
                    },
                    CRCHPART_BSCLITRGY: {
                        type: "string",
                        editable: false
                    },
                    CRCHPART_SOCTEACH: {
                        type: "string",
                        editable: false
                    },
                    CRCHPART_EUCHLIFE: {
                        type: "string",
                        editable: false
                    },
                    CRCHPART_OTHER: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (E2): Synchornization complete!");
            }
    });
    $("#tbl-e2_1").kendoGrid({
        dataSource: churchpart_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Family Member (Name)",
            field: "CHRCHPART_NAME",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            title: "Evangelization",
            field: "CHRCHPART_EVANGELIC",
            width: "250px"
        }, {
            title: "GKK Orientation",
            field: "CRCHPART_GKKORIENT",
            width: "250px"
        }, {
            title: "Basic Bible",
            field: "CRCHPART_BSCBIBLE",
            width: "250px"
        }, {
            title: "Basic Liturgy",
            field: "CRCHPART_BSCLITRGY",
            width: "250px"
        }, {
            title: "Social Teaching",
            field: "CRCHPART_SOCTEACH",
            width: "250px"
        }, {
            title: "Life in the Eucharist",
            field: "CRCHPART_EUCHLIFE",
            width: "250px"
        }, {
            title: "Other",
            field: "CRCHPART_OTHER",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });

    //E.3 - Seminars
    var seminars_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/e3-table.php",
            create: {
                url: "tables/backend/e3-table.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING e3: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e3: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/e3-table.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING e3: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e3: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/e3-table.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING e3: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING e3: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    SEMNR_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    SEMNR_NAME: {
                        type: "string",
                        editable: false
                    },
                    SEMNR_TOPIC: {
                        type: "string",
                        editable: false
                    },
                    SEMNR_DATE: {
                        type: "string",
                        editable: false
                    },
                    SEMNR_CONDUCTD: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (E3): Synchornization complete!");
            }
    });
    $("#tbl-e3_1").kendoGrid({
        dataSource: seminars_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Title of Seminar/ Training Program",
            field: "SEMNR_NAME",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            title: "Topic/Subject",
            field: "SEMNR_TOPIC",
            width: "250px"
        }, {
            title: "Date Taken",
            field: "SEMNR_DATE",
            width: "250px"
        }, {
            title: "Conducted By",
            field: "SEMNR_CONDUCTD",
            width: "250px"
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });


   //F.1
    var feedbackf1_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/feedback_f1.php",
            create: {
                url: "tables/backend/feedback_f1.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING f1: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f1: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/feedback_f1.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING f1: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f1: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/feedback_f1.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING f1: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f1: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    FEEDBACK_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    FEEDBACK: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (F1): Synchornization complete!");
            }
    });
    $("#tbl-f1").kendoGrid({
        dataSource: feedbackf1_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Feedback",
            field: "FEEDBACK",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });

   //F.2
    var feedbackf2_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/feedback_f2.php",
            create: {
                url: "tables/backend/feedback_f2.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING f2: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f2: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/feedback_f2.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING f2: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f2: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/feedback_f2.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING f2: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f2: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    FEEDBACK_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    FEEDBACK: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (F2): Synchornization complete!");
            }
    });
    $("#tbl-f2").kendoGrid({
        dataSource: feedbackf2_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Feedback",
            field: "FEEDBACK",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });


   //F.3
    var feedbackf3_ds = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/feedback_f3.php",
            create: {
                url: "tables/backend/feedback_f3.php",
                type: "PUT",
                datatype: "json",
                complete: function(e) {
                    console.log("DEBUGGING f3: ENTRY ADDED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f3: ADD ENTRY FAILED.");
                }
            },
            update: {
                url: "tables/backend/feedback_f3.php",
                type: "POST",
                complete: function(e) {
                    console.log("DEBUGGING f3: ENTRY UPDATED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f3: UPDATE ENTRY FAILED.");
                }
            },
            destroy: {
                url: "tables/backend/feedback_f3.php",
                type: "DELETE",
                complete: function(e) {
                    console.log("DEBUGGING f3: ENTRY DELETED.");
                },
                error: function(e) {
                    console.log("DEBUGGING f3: DELETE ENTRY FAILED.");
                }
            }
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    FEEDBACK_NUM: {
                        editable: false,
                        nullable: true
                    },
                    RSPONDNT_NUM: {
                        editable: false,
                        nullable: true
                    },
                    SURVYR_NUM: {
                        editable: false,
                        nullable: false
                    },
                    QN_NUMBR: {
                        editable: false,
                        nullable: true
                    },
                    FEEDBACK: {
                        type: "string",
                        editable: false
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (f3): Synchornization complete!");
            }
    });
    $("#tbl-f3").kendoGrid({
        dataSource: feedbackf3_ds,
        height: 300,
        selectable: true,
        editable: true,
        scrollable: true,
        columns: [{
            title: "Feedback",
            field: "FEEDBACK",
            width: "250px"
        }, {
            title: "RSPONDNT_NUM",
            field: "RSPONDNT_NUM",
            hidden: true
        }, {
            title: "QN_NUMBER",
            field: "QN_NUMBER",
            hidden: true
        }, {
            command: ["destroy"],
            title: "&nbsp;",
            width: "250px"
        }]
    });

   //Custom Data
    var max_entries_user = new kendo.data.DataSource({
        transport: {
            read: "tables/backend/max_num_records.php"
        },
        schema: {
            model: {
                id: "id",
                fields: {
                    id: {
                        type: "number"
                    },
                    UserID: {
                        type: "string"
                    },
                    Name: {
                        type: "string"
                    },
                    Count: {
                        type: "string"
                    }
                }
            }
        },
            sync: function(e) {
                 console.log("DEBUGGING (f3): Synchornization complete!");
            }
    });
    $("#max_entries_user").kendoGrid({
        dataSource: max_entries_user,
        height: 150,
        scrollable: true,
        columns: [{
            title: "User ID",
            field: "UserID",
            width: "250px"
        },{
            title: "Name",
            field: "Name",
            width: "250px"
        }, {
            title: "Entries created",
            field: "Count",
            width: "250px"
        }]
    });


});