/*



Input masks using jquery.inputmasks.js

    ~ John Santiago



 */
$(document).ready(function() {
    /*    BASIS

    $('#interviewer_name').inputmask('Regex', {

        regex: "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\\.[a-zA-Z]{2,4}"

    });

    */
    $('#interviewer_name').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,70}"
    });
    $('#qn_num').inputmask('Regex', {
        regex: "[0-9]{2,15}"
    });
    $('#a1_fullname').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,70}"
    });
    $('#a3_fulladdr').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s.,-]{2,150}"
    });
    $('#e1_1-why').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s.,]{2,150}"
    });
    $('#e1_2-why').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s.,]{2,150}"
    });
    $('#e1_3-why').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s.,]{2,150}"
    });
    $("#f1_textboxgroup").on('focus', 'input', function() {
        $(this).inputmask('Regex', {
            regex: "[0-9a-zA-Z\\s]{2,150}"
        });
    });
    $("#f2_textboxgroup").on('focus', 'input', function() {
        $(this).inputmask('Regex', {
            regex: "[0-9a-zA-Z\\s]{2,150}"
        });
    });
    $("#f3_textboxgroup").on('focus', 'input', function() {
        $(this).inputmask('Regex', {
            regex: "[0-9a-zA-Z\\s]{2,150}"
        });
    });
    //B.1 Modal
    $.extend($.inputmask.defaults.definitions, {
        'A': {
            validator: "[0A-Ka-k]",
            cardinality: 1,
            casing: "upper"
        },
        'B': {
            validator: "[RCIPNBAJWOrcipnbajwo]",
            cardinality: 1,
            casing: "upper"
        },
        'C': {
            validator: "[SMWPLGsmwplg]",
            cardinality: 1,
            casing: "upper"
        }
    });
    $('#tbl-b1_name').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b1-bday').inputmask('Regex', {
        regex: "[0-9a-zA-Z.-]{2,150}"
    });
    $('#tbl-b1_edu').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b1_occu').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b1_civstat').inputmask('Regex', {
        mask: "C",
        repeat: 3
    });
    $('#tbl-b1_income').inputmask({
        mask: "A"
    });
    $('#tbl-b1_religion').inputmask('Regex', {
        mask: "B",
        repeat: 3
    });
    $('#tbl-b1_numbirthtotal').inputmask('Regex', {
        regex: "[0-9]{1,4}"
    });
    $('#tbl-b1_numbirthalive').inputmask('Regex', {
        regex: "[0-9]{1,4}"
    });
    $('#tbl-b1_residency').inputmask('Regex', {
        regex: "[0-9]{1,4}"
    });
    $('#tbl-b1_tribe').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    //B.2 Modal
    $('#tbl-b2_name').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b2_civstat').inputmask('Regex', {
        mask: "C",
        repeat: 3
    });
    $('#tbl-b2_nutri').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b2-bday').inputmask('Regex', {
        regex: "[0-9a-zA-Z.-]{2,150}"
    });
    $('#tbl-b2_immu').inputmask('Regex', {
        regex: "[a-zA-Z\\s,]{2,150}"
    });
    $('#tbl-b2_edu').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b2-features').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    //B.3 Modal
    $('#tbl-b3_item').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-b3_other').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    //C.1 Modal
    $('#tbl-c1_name').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-c1_ofwyears').inputmask('Regex', {
        regex: "[0-9\\s]{2,150}"
    });
    $('#tbl-c1_IsOFWOrgMember').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    //D.1 Modal
    $('#tbl-d1_name').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-d1_skills').inputmask('Regex', {
        regex: "[a-zA-Z\\s,]{2,150}"
    });
    $('#tbl-d1_training').inputmask('Regex', {
        regex: "[a-zA-Z\\s,]{2,150}"
    });
    $('#tbl-d1_preferskills').inputmask('Regex', {
        regex: "[a-zA-Z\\s,]{2,150}"
    });
    //E.2 Modal
    $('#tbl-e2_name').inputmask('Regex', {
        regex: "[a-zA-Z\\s]{2,150}"
    });
    $('#tbl-e2_others').inputmask('Regex', {
        regex: "[a-zA-Z\\s,]{2,150}"
    });
    //E.3 Modal
    $('#tbl-e3_title').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s]{2,150}"
    });
    $('#tbl-e3_topic').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s]{2,150}"
    });
    $('#tbl-e3_conduct').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s]{2,150}"
    });
    $('#tbl-e3_date').inputmask('Regex', {
        regex: "[0-9a-zA-Z\\s]{2,150}"
    });
    //LOGIN FORM
    $('#uname').inputmask('Regex', {
        regex: "[0-9a-zA-Z]{2,150}"
    });
    $('#pword').inputmask('Regex', {
        regex: "[0-9a-zA-Z]{2,150}"
    });
});