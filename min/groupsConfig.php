<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'fonts' => array('//assets/css/font-icons/entypo/css/entypo.css', '//assets/css/font-icons/entypo/css/animation.css', '//assets/fonts/banda_regular_macroman/stylesheet.css', '//assets/fonts/octincollege_regular_macroman/stylesheet.css'),
    'mycss' => array('//assets/css/neon.css', '//assets/css/custom.css'),
    'endcss' => array('//assets/js/zurb-responsive-tables/responsive-tables.css', '//assets/js/select2/select2.css', '//assets/js/select2/select2-bootstrap.css', '//assets/js/selectboxit/jquery.selectBoxIt.css'),
    'jquery' => array('//assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js', '//assets/js/jquery.bootstrap.wizard.min.js', '//assets/js/jquery.validate.min.js', '//assets/js/jquery.inputmask.bundle.min.js', '//assets/js/selectboxit/jquery.selectBoxIt.min.js', '//assets/js/bootstrap-timepicker.min.js', '//assets/js/select2/select2.min.js', '//assets/js/jquery.multi-select.js'),
    'bootstrap' => array('//assets/js/bootstrap.min.js', '//assets/js/bootstrap-datepicker.js', '//assets/js/bootstrap-switch.min.js'),
    'custom1' => array('//assets/js/chance.js', '//extrajs.js', '//assets/js/gsap/main-gsap.js', '//assets/js/joinable.js', '//assets/js/resizeable.js', '//assets/js/neon-api.js', '//assets/js/neon-custom.js', '//assets/js/zurb-responsive-tables/responsive-tables.js'),
    // 'css' => array('//css/file1.css', '//css/file2.css'),
);