<?php
session_start();
if(isset($_SESSION['usr_num'])) {
    header("Location: /wizard");
}
include $_SERVER['DOCUMENT_ROOT'] . '/top_page_comment.html';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Login | 001A</title>
        <meta name="description" content="Classified. Access denied." />
        <meta name="author" content="John Santiago" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <!-- JQUERY -->
        <script src="/assets/js/jquery-1.11.0.min.js"></script>
        <link rel="stylesheet" href="/assets/js/jquery-ui/jquery-ui-1.10.3.custom.min.css">
        <script src="/assets/js/jquery-ui/jquery-ui-1.10.3.minimal.min.js"></script>

        <?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/important_scripts.php'; ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->
        <script type='text/javascript'>
            $(document).ready(function() {
                var neonLogin = neonLogin || {};
                neonLogin.$container = $("#form_login");
                $('#username').inputmask('Regex', {
                    regex: "[0-9a-zA-Z_]{2,50}"
                });
                $('#password').inputmask('Regex', {
                    regex: "[0-9a-zA-Z_]{2,50}"
                });
                $("#form_login").submit(function(e) {
                    if ($("#form_login").valid() === true) {
                    e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: "login.php",
                                data: $(this).serialize(),
                                dataType: 'json',
                                success: function(data) {
                                    var retusrname = data[0].USERNAME;
                                    var retpasswrd = data[0].PASSWORD;
                                    var uname = $('#username').val();
                                    var pword = $('#password').val();
                                    if (retusrname == uname && retpasswrd == pword) {
                                        $(".login-page").addClass("logging-in");
                                        setTimeout(function() {
                                            var random_pct = 25 + Math.round(Math.random() * 30);
                                            neonLogin.setPercentage(random_pct, function() {
                                                setTimeout(function() {
                                                    neonLogin.setPercentage(100, function() {
                                                        setTimeout(function() {document.location.href='/wizard';}, 500);
                                                    }, 2);
                                                }, 820);
                                            });
                                        }, 650);
                                    }
                                    else {
                                        $(".login-page").removeClass("logging-in");
                                        $('input[type="submit"]').removeAttr('disabled');
                                        //$("#add_err").html("Invalid username or password.");
                                    }
                                }
                            });
                    }
                });
        var is_lockscreen = $(".login-page").hasClass("is-lockscreen");
        neonLogin.$body = $(".login-page");
        neonLogin.$login_progressbar_indicator = $(".login-progressbar-indicator h3");
        neonLogin.$login_progressbar = neonLogin.$body.find(".login-progressbar div");
        neonLogin.$login_progressbar_indicator.html("0%");
        if (neonLogin.$body.hasClass("login-form-fall")) {
            var focus_set = false;
            setTimeout(function() {
                neonLogin.$body.addClass("login-form-fall-init");
                setTimeout(function() {
                    if (!focus_set) {
                        neonLogin.$container.find("input:first").focus();
                        focus_set = true;
                    }
                }, 550);
            }, 0);
        } else {
            neonLogin.$container.find("input:first").focus();
        }
        neonLogin.$container.find(".form-control").each(function(i, el) {
            var $this = $(el), $group = $this.closest(".input-group");
            $this.prev(".input-group-addon").click(function() {
                $this.focus();
            });
            $this.on({
                focus: function() {
                    $group.addClass("focused");
                },
                blur: function() {
                    $group.removeClass("focused");
                }
            });
        });
        $.extend(neonLogin, {
            setPercentage: function(pct, callback) {
                pct = parseInt(pct / 100 * 100, 10) + "%";
                if (is_lockscreen) {
                    neonLogin.$lockscreen_progress_indicator.html(pct);
                    var t = {
                        pct: currentProgress
                    };
                    TweenMax.to(t, 0.7, {
                        pct: parseInt(pct, 10),
                        roundProps: [ "pct" ],
                        ease: Sine.easeOut,
                        onUpdate: function() {
                            neonLogin.$lockscreen_progress_indicator.html(t.pct + "%");
                            drawProgress(parseInt(t.pct, 10) / 100);
                        },
                        onComplete: callback
                    });
                    return;
                }
                neonLogin.$login_progressbar_indicator.html(pct);
                neonLogin.$login_progressbar.width(pct);
                var o = {
                    pct: parseInt(neonLogin.$login_progressbar.width() / neonLogin.$login_progressbar.parent().width() * 100, 10)
                };
                TweenMax.to(o, 0.7, {
                    pct: parseInt(pct, 10),
                    roundProps: [ "pct" ],
                    ease: Sine.easeOut,
                    onUpdate: function() {
                        neonLogin.$login_progressbar_indicator.html(o.pct + "%");
                    },
                    onComplete: callback
                });
            }
        });
            //$("#form_login").validate();
            })(jQuery, window);
        </script>
    </head>
    <body class="page-body login-page login-form-fall">
        <div class="login-container">
            <div class="login-header login-caret">
                <div class="login-content">
                    <a href="javascript: " class="logo">
                    <img src="/assets/images/logo%402x.png" width="320" alt="" />
                    </a>
                    <p class="description">Developed by John Michael Santiago.</p>
                    <!-- progress bar indicator -->
                    <div class="login-progressbar-indicator">
                        <h3>43%</h3>
                        <span>Using unsecured login...</span>
                    </div>
                </div>
            </div>
            <div class="login-progressbar">
                <div></div>
            </div>
            <div class="login-form">
                <div class="login-content">
                    <form method="post" role="form" id="form_login">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="entypo-user"></i>
                                </div>
                                <input type="text" class="form-control" name="username" id="username" placeholder="Username" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="entypo-key"></i>
                                </div>
                                <input type="password" class="form-control" name="password" id="password" placeholder="Password" />
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block btn-login">
                            Sign in
                            <i class="entypo-login"></i>
                            </button>
                        </div>
                    </form>
                    <div class="login-bottom-links">
                        <a href="/" class="link">Home</a>
                        <br />
                    </div>
                </div>
            </div>
        </div>
<!-- FORM VALIDATION PLUGIN -->
<script src="/assets/js/jquery.validate.js"></script>

<!-- INPUT MASK PLUGIN -->
<script src="/assets/js/jquery.inputmask.bundle.js"></script>

<script src="/assets/js/gsap/main-gsap.js"></script>
    </body>
</html>