<!-- set fixed position by adding class "navbar-fixed-top" -->
<header class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <!-- logo -->
        <div class="navbar-brand">
            <a href="/wizard">
            <img src="/assets/images/logo%402x.png" width="88" alt="" />
            </a>
        </div>
        <!-- main menu -->
        <ul class="navbar-nav ">
            <li id="reports_page">
                <a href="javascript: "><i class="entypo-gauge"></i><span>Survey Data</span></a>
            </li>
            <li>
                <a href="/wizard"><i class="entypo-newspaper"></i><span>Survey Form</span></a>
            </li>
            <li>
                <a href="javascript: "><i class="entypo-layout"></i><span>Account</span></a>
                <ul>
                    <li id="account_stats">
                        <a href="javascript: "><span>Statistics</span></a>
                    </li>
                    <li>
                        <a href="/logout"><span>Logout</span></a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- notifications and other links -->
        <ul class="nav navbar-right pull-right">
            <!--
            <li>
                <a href="#">
                Log Out <i class="entypo-logout right"></i>
                </a>
            </li>
            -->
            <!-- mobile only -->
            <li class="visible-xs">
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="horizontal-mobile-menu visible-xs">
                    <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>