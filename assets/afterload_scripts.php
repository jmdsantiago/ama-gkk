
<!-- KENDO UI WEB -->
<link rel="stylesheet" href="/assets/css/kendo/kendo.common-bootstrap.css">
<link rel="stylesheet" href="/assets/css/kendo/kendo.rtl.css">
<link rel="stylesheet" href="/assets/css/kendo/kendo.bootstrap.css">
<script src="/assets/js/kendo.web.js"></script>

<!-- SELECT2 PLUGIN -->
<link rel="stylesheet" href="/assets/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="/assets/js/select2/select2.css">
<script src="/assets/js/select2/select2.js"></script>

<!-- GSAP PLUGIN -->
<script src="/assets/js/gsap/main-gsap.js"></script>

<!-- RANDOMIZER PLUGIN -->
<script src="/assets/js/chance.js"></script>

<!-- Dynatable -->
<link rel="stylesheet" href="/assets/js/dynatable/jquery.dynatable.css">
<script src="/assets/js/dynatable/jquery.dynatable.js"></script>

<!-- WIZARD PLUGIN -->
<script src="/assets/js/jquery.bootstrap.wizard.js"></script>

<!-- FORM VALIDATION PLUGIN -->
<script src="/assets/js/jquery.validate.js"></script>

<!-- INPUT MASK PLUGIN -->
<script src="/assets/js/jquery.inputmask.bundle.js"></script>

<!-- DATE PICKER (SINGLE) -->
<script src="/assets/js/datepicker/bootstrap-datepicker.js"></script>

<!-- BOOTSTRAP SWITCHES CONVERT PLUGIN (RADIO BUTTONS AND CHECKBOXES) -->
<link rel="stylesheet" href="/assets/js/switch/bootstrap-switch.css">
<script src="/assets/js/switch/bootstrap-switch.js"></script>

<!-- MULTI-SELECT PLUGIN -->
<link rel="stylesheet" href="/assets/js/multi-select/multi-select.css">
<script src="/assets/js/multi-select/jquery.multi-select.js"></script>

<!-- SELECT BOX REPLACE PLUGIN -->
<link rel="stylesheet" href="/assets/js/selectboxit/jquery.selectBoxIt.css">
<script src="/assets/js/selectboxit/jquery.selectBoxIt.js"></script>

<!-- TIME PICKER -->
<script src="/assets/js/timepicker/bootstrap-timepicker.js"></script>

<!-- TABLE OF CONTENTS -->
<link rel="stylesheet" href="/assets/js/table-of-contents/jquery.tocify.css">
<script src="/assets/js/table-of-contents/jquery.tocify.js"></script>

<!-- CHARTS, GRAPHS -->
<script src="/assets/js/visual-data/jquery.peity.js"></script>
<script src="/assets/js/visual-data/jquery.knob.js"></script>
<script src="/assets/js/visual-data/jquery.sparkline.js"></script>
<link rel="stylesheet" href="/assets/js/visual-data/morris/morris.css">
<script src="/assets/js/visual-data/morris/morris.js"></script>
<script src="/assets/js/visual-data/morris/raphael.js"></script>
<link rel="stylesheet" href="/assets/js/visual-data/rickshaw/rickshaw.min.css">
<script src="/assets/js/visual-data/rickshaw/rickshaw.min.js"></script>

<!-- CSS3 FIX  -->
<script src="/assets/js/respond.js"></script>

<!-- TOAST NOTIFICATIONS -->
<link rel="stylesheet" href="/assets/js/toastr/toastr.css">
<script src="/assets/js/toastr/toastr.js"></script>

<!-- AUTOCOMPLETE -->
<script src="/assets/js/typeahead.bundle.js"></script>

<!-- THEME PLUGINS -->
<script src="/assets/js/joinable.js"></script>
<script src="/assets/js/resizeable.js"></script>
<script src="/assets/js/neon-api.js"></script>
<script src="/assets/js/cookies.js"></script>
<script src="/assets/js/neon-custom.js"></script>