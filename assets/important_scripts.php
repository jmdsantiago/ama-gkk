<!-- CSS -->
<link rel="stylesheet" href="/assets/css/custom.css">
<link rel="stylesheet" href="/assets/css/boogs.css">
<link rel="stylesheet" href="/assets/css/animate.css">

<!-- FONTS -->
<!--
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'> -->
<link rel="stylesheet" href="/assets/css/fonts/notosans_regular_macroman/stylesheet.css">
<link rel="stylesheet" href="/assets/css/fonts/octincollege_regular_macroman/stylesheet.css">
<link rel="stylesheet" href="/assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="/assets/css/font-icons/entypo/css/animation.css">
<link rel="stylesheet" href="/assets/css/font-icons/font-awesome/css/font-awesome.css">

<!-- JQUERY -->
<script src="/assets/js/jquery-1.11.0.min.js"></script>
<link rel="stylesheet" href="/assets/js/jquery-ui/jquery-ui-1.10.3.custom.min.css">
<script src="/assets/js/jquery-ui/jquery-ui-1.10.3.minimal.min.js"></script>

<!-- BOOTSTRAP -->
<script src="/assets/js/bootstrap.js"></script>
