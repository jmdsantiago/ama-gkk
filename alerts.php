<div class="col-md-12">
	<div class="alert alert-info">
	<br>
	<h4>For Your Information</h4>
	<ol>
		<li><p>All values that are left blank or " - " can be considered as "None".</p></li>
		<li><p>All values that have a check can be considered as "Yes" or "True".</p></li>
		<li><p>If a civil status value contains an "L I" or "LI", assume "SGP" civil status if there is a child present in table B.2. Otherwise, assume "S" civil status.</p></li>
		<li><p>If a person has a civil status of "W" and has listed his/her partner as "Deceased", then the civil status of said partner is assumed to be "M". Which is the last civil status of the person before passing away.</p></li>
		<li><p>If the number written under "Number of Births" and "Number of Births Alive" is in an unbelievable amount, then count the number of children written in table B.2 (if applicable) as the correct value. Otherwise, input 0.</p></li>
		<li><p>If there are no values present for "Date of Interview", "Time Started" and "Time Ended", then make up your own values. Make sure the value for "Date of Interview" is any date between December 1 2013 to March 2014.</p></li>
	</ol>
	</div>
</div>