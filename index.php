<?php 
session_start(); 
if(!isset($_SESSION['usr_num'])) {
	header("Location: /login");
}
else{
	header("Location: /wizard");
}
include 'top_page_comment.html'; 
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<script src="/assets/preloader/pace.min.js"></script>
		<link rel="stylesheet" href="/assets/preloader/pace.css">
		<meta charset="utf-8">
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="Preview version. Anything you see here may not be included in the final version of the site." />
		<meta name="author" content="John Santiago" />
		<title>Home | Automated Survey beta</title>
		<?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/important_scripts.php'; ?>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="page-body page-fade-only">
		<div class="page-container horizontal-menu">
			<!-- SIDEBAR -->
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/header.php'; ?>
			<div class="main-content">
				<!--PLACE ALERTS HERE-->
				<ol class="breadcrumb bc-3">
					<li class="active">
						<a href="/wizard"><i class="entypo-home"></i><strong>Home</strong></a>
					</li>
				</ol>
				<br/>
				</div>
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/footer.php'; ?>
		</div>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/afterload_scripts.php'; ?>
<script src="/reports/visual-data.js"></script>
	</body>
</html>