/*



Extra Javascript (JQuery) scripts

    ~ John Santiago



 */

// FOR VIEWING KENDO UI GRID DATA (DEBUGGING)
 /*displayedData = dataSource.view();
displayedDataAsJSON = JSON.stringify(displayedData);
alert(displayedDataAsJSON);
*/
$(document).ready(function() {

        // GENERATE RESPONDENT number
        rspondnt_num = chance.natural({min: 1, max: 99999999});
        $('#rspondnt_num').val(rspondnt_num);
        // GENERATE CURRENT DATE
        $("#registered_date").datepicker("setDate", new Date());

    // OFW PAGE (events)
    $("#rd-ofw-1-lbl").click(function() {
        $("#tbl-ofw-container").fadeIn();
        $("#tbl-ofw-container :button").prop('disabled', false);
    });
    $("#rd-ofw-2-lbl").click(function() {
        $("#tbl-ofw-container").fadeOut();
        $("#tbl-ofw-container :button").prop('disabled', true);
    });
    $("#rd-ofw-1").click(function() {
        $("#tbl-ofw-container").fadeIn();
        $("#tbl-ofw-container :button").prop('disabled', false);
    });
    $("#rd-ofw-2").click(function() {
        $("#tbl-ofw-container").fadeOut();
        $("#tbl-ofw-container :button").prop('disabled', true);
    });
    // E. INVOLVEMENT IN PARISH (events)
    $("#rd-e1-2-lbl-baptism").click(function() {
        $("#tbl-e1_1-container").fadeIn();
        $("#tbl-e1_1-container :input").prop('disabled', false);
    });
    $("#rd-e1-1-lbl-baptism").click(function() {
        $("#tbl-e1_1-container").fadeOut();
        $("#tbl-e1_1-container :input").prop('disabled', true);
    });
    $("#rd-e1-2-baptism").click(function() {
        $("#tbl-e1_1-container").fadeIn();
        $("#tbl-e1_1-container :input").prop('disabled', false);
    });
    $("#rd-e1-1-baptism").click(function() {
        $("#tbl-e1_1-container").fadeOut();
        $("#tbl-e1_1-container :input").prop('disabled', true);
    });


    $("#rd-e1-2-lbl-confirm").click(function() {
        $("#tbl-e1_2-container").fadeIn();
        $("#tbl-e1_2-container :input").prop('disabled', false);
    });
    $("#rd-e1-1-lbl-confirm").click(function() {
        $("#tbl-e1_2-container").fadeOut();
        $("#tbl-e1_2-container :input").prop('disabled', true);
    });
    $("#rd-e1-2-confirm").click(function() {
        $("#tbl-e1_2-container").fadeIn();
        $("#tbl-e1_2-container :input").prop('disabled', false);
    });
    $("#rd-e1-1-confirm").click(function() {
        $("#tbl-e1_2-container").fadeOut();
        $("#tbl-e1_2-container :input").prop('disabled', true);
    });

    $("#rd-e1-1-lbl").click(function() {
        $("#tbl-e1_3-container").fadeIn();
        $("#tbl-e1_3-container :input").prop('disabled', false);
    });
    $("#rd-e1-3-lbl").click(function() {
        $("#tbl-e1_3-container").fadeIn();
        $("#tbl-e1_3-container :input").prop('disabled', false);
    });
    $("#rd-e1-1-marriage").click(function() {
        $("#tbl-e1_3-container").fadeIn();
        $("#tbl-e1_3-container :input").prop('disabled', false);
    });
    $("#rd-e1-3-marriage").click(function() {
        $("#tbl-e1_3-container").fadeIn();
        $("#tbl-e1_3-container :input").prop('disabled', false);
    });
    $("#rd-e1-2-lbl").click(function() {
        $("#tbl-e1_3-container").fadeOut();
        $("#tbl-e1_3-container :input").prop('disabled', true);
    });
    $("#rd-e1-2-marriage").click(function() {
        $("#tbl-e1_3-container").fadeOut();
        $("#tbl-e1_3-container :input").prop('disabled', true);
    });

    $("#rd-e1_2-2-lbl").click(function() {
        $("#tbl-e1_2-container").fadeIn();
        $("#tbl-e1_2-container :input").prop('disabled', false);
    });
    $("#rd-e1_2-1-lbl").click(function() {
        $("#tbl-e1_2-container").fadeOut();
        $("#tbl-e1_2-container :input").prop('disabled', true);
    });
    $("#rd-e1_2-2").click(function() {
        $("#tbl-e1_2-container").fadeIn();
        $("#tbl-e1_2-container :input").prop('disabled', false);
    });
    $("#rd-e1_2-1").click(function() {
        $("#tbl-e1_2-container").fadeOut();
        $("#tbl-e1_2-container :input").prop('disabled', true);
    });
    $("#rd-e1_3-2-lbl").click(function() {
        $("#tbl-e1_3-container").fadeIn();
        $("#tbl-e1_3-container :input").prop('disabled', false);
    });
    $("#rd-e1_3-1-lbl").click(function() {
        $("#tbl-e1_3-container").fadeOut();
        $("#tbl-e1_3-container :input").prop('disabled', true);
    });
    $("#rd-e1_3-2").click(function() {
        $("#tbl-e1_3-container").fadeIn();
        $("#tbl-e1_3-container :input").prop('disabled', false);
    });
    $("#rd-e1_3-1").click(function() {
        $("#tbl-e1_3-container").fadeOut();
        $("#tbl-e1_3-container :input").prop('disabled', true);
    });
    $("#rd-e2_1-1-lbl").click(function() {
        $("#tbl-e2_1-container").fadeIn();
    });
    $("#rd-e2_1-2-lbl").click(function() {
        $("#tbl-e2_1-container").fadeOut();
    });
    $("#rd-e2_1-1").click(function() {
        $("#tbl-e2_1-container").fadeIn();
    });
    $("#rd-e2_1-2").click(function() {
        $("#tbl-e2_1-container").fadeOut();
    });
    $("#rd-e3_1-1-lbl").click(function() {
        $("#tbl-e3_1-container").fadeIn();
    });
    $("#rd-e3_1-2-lbl").click(function() {
        $("#tbl-e3_1-container").fadeOut();
    });
    $("#rd-e3_1-1").click(function() {
        $("#tbl-e3_1-container").fadeIn();
    });
    $("#rd-e3_1-2").click(function() {
        $("#tbl-e3_1-container").fadeOut();
    });

    $("#rd-b2-1-2-lbl").click(function() {
        $("#b2_IsInSchool_container").fadeIn();
        $("#b2_IsInSchool_container :input").prop('disabled', false);
    });
    $("#rd-b2-1-1-lbl").click(function() {
        $("#b2_IsInSchool_container").fadeOut();
        $("#b2_IsInSchool_container :input").prop('disabled', true);
    });
    $("#rd-b2-2-school").click(function() {
        $("#b2_IsInSchool_container").fadeIn();
        $("#b2_IsInSchool_container :input").prop('disabled', false);
    });
    $("#rd-b2-1-school").click(function() {
        $("#b2_IsInSchool_container").fadeOut();
        $("#b2_IsInSchool_container :input").prop('disabled', true);
    });

    $("#rd-a0-1-interview").click(function() {
       $('#interview_value').val("First");
    });
    $("#rd-a0-2-interview").click(function() {
       $('#interview_value').val("Second");
    });
    $("#rd-a0-1-lbl").click(function() {
       $('#interview_value').val("First");
    });
    $("#rd-a0-2-lbl").click(function() {
       $('#interview_value').val("Second");
    });

    $("#rd-a1-1-gender").click(function() {
       $('#gender').val("Male");
    });
    $("#rd-a1-2-gender").click(function() {
       $('#gender').val("Female");
    });
    $("#rd-a1-1-lbl").click(function() {
       $('#gender').val("Male");
    });
    $("#rd-a1-2-lbl").click(function() {
       $('#gender').val("Female");
    });

    //B.3 Modal House Build
    $("#rd-b3-build-concrete").click(function() {
        $("#b3_other_container").fadeOut();
        $("#b3_other_container :input").prop('disabled', true);
    });
    $("#rd-b3-build-semiconcrete").click(function() {
        $("#b3_other_container").fadeOut();
        $("#b3_other_container :input").prop('disabled', true);
    });
    $("#rd-b3-build-wood").click(function() {
        $("#b3_other_container").fadeOut();
        $("#b3_other_container :input").prop('disabled', true);
    });
    $("#rd-b3-build-other").click(function() {
        $("#b3_other_container").fadeIn();
        $("#b3_other_container :input").prop('disabled', false);
    });
    $("#rd-b3-lbl-build-concrete").click(function() {
        $("#b3_other_container").fadeOut();
        $("#b3_other_container :input").prop('disabled', true);
    });
    $("#rd-b3-lbl-build-semiconcrete").click(function() {
        $("#b3_other_container").fadeOut();
        $("#b3_other_container :input").prop('disabled', true);
    });
    $("#rd-b3-lbl-build-wood").click(function() {
        $("#b3_other_container").fadeOut();
        $("#b3_other_container :input").prop('disabled', true);
    });
    $("#rd-b3-lbl-build-other").click(function() {
        $("#b3_other_container").fadeIn();
        $("#b3_other_container :input").prop('disabled', false);
    });

    //B.1 Modal

    function showb1modal() {
        $('#modal-b1').modal('show', {
            backdrop: 'static'
        });
    }

    $("#btn-b1-add").click(function() {
        showb1modal();
        $('#tbl-b1_name').val("");
        $('#tbl-b1-bday').val("");
        $('#tbl-b1_civstat').val("");
        $('#tbl-b1_edu').val("");
        $('#tbl-b1_occu').val("");
        $('#tbl-b1_income').val("");
        $('#tbl-b1_religion').val("");
        $('#tbl-b1_numbirthtotal').val("");
        $('#tbl-b1_numbirthalive').val("");
        $('#tbl-b1_residency').val("");
        $('#tbl-b1_tribe').val("");
    });

    /*B.1 RADIO BUTTONS
    $('.b1').on("click", "tr", function() {
        $(this).find('td input[type="radio"]').prop('checked', true);
    }); */

    //B.1 MODAL VALIDATION
    $("#b1form").validate();
    var b1isvalid;
    $("#tbl-b1_name").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_edu").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "Type 'None' if this person has no educational background.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-b1-bday").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_occu").rules("add", {
        required: true,
        minlength: 3,
        messages: {
            required: "Type 'None' if this person has no occupation.",
            minlength: jQuery.format(" At least {0} characters are necessary")
        }
    });
    $("#tbl-b1_civstat").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_income").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_religion").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_numbirthtotal").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_numbirthalive").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_residency").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b1_tribe").rules("add", {
        required: true,
        minlength: 3,
        messages: {
            required: "Type 'None' if this person has no tribe of origin.",
            minlength: jQuery.format(" At least {0} characters are necessary")
        }
    });
    $("#tbl-b1-save").click(function() {
        $("#b1form").valid();
        if ($("#b1form").valid() === true) {
            var td1 = $('#tbl-b1_name').val();
            var td2;
            if ($('#rd-b1-1-gender').is(':checked')) {
                td2 = "Male";
            }
            if ($('#rd-b1-2-gender').is(':checked')) {
                td2 = "Female";
            }
            var td3 = $('#tbl-b1-bday').val();
            var td4 = $('#tbl-b1_civstat').val();
            var td5 = $('#tbl-b1_edu').val();
            var td6 = $('#tbl-b1_occu').val();
            var td7 = $('#tbl-b1_income').val();
            var td8 = $('#tbl-b1_religion').val();
            var td9;
            if ($('#rd-b1-1-marriage').is(':checked')) {
                td9 = "Civil";
            }
            if ($('#rd-b1-2-marriage').is(':checked')) {
                td9 = "Church";
            }
            if ($('#rd-b1-3-marriage').is(':checked')) {
                td9 = "Civil and Church";
            }
            if ($('#rd-b1-4-marriage').is(':checked')) {
                td9 = "None";
            }
            var td10 = $('#tbl-b1_numbirthtotal').val();
            var td11 = $('#tbl-b1_numbirthalive').val();
            var td12 = $('#tbl-b1_residency').val();
            var td13 = $('#tbl-b1_tribe').val();

            var qn_num = $('#qn_num').val();
            var parnt_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var b1 = $('#tbl-fmlyhousehld').data('kendoGrid');
            var b1datasource = b1.dataSource;
            b1datasource.insert({ PARNT_NUM: parnt_num, SURVYR_NUM: survyr_num, RSPONDNT_NUM: rspondnt_num , QN_NUMBER: qn_num, PARNT_NAME: td1, PARNT_SEX: td2, PARNT_BIRTHDATE: td3, PARNT_CIVSTATUS: td4, PARNT_EDU: td5, PARNT_OCCUPATION: td6, PARNT_MONTHINC: td7, PARNT_RELIGION: td8, PARNT_MARRIAGE: td9, PARNT_NUMBIRTHS: td10, PARNT_NUMBIRTHSALIVE: td11, PARNT_RESIDENCEYEARS: td12, PARNT_TRIBE: td13 });

            //DEBUGGING PURPOSES
            console.log('B1 Entry has been added.');

            //OLD HTML TABLE INSERT CODE
            //$('#tbl-fmlyhousehld tr:last').after('<tr id="b1-' + b1ctr + '"><td>' + td1 + '</td><td>' + td2 + '</td><td>' + td3 + '</td><td>' + td4 + '</td><td>' + td5 + '</td><td>' + td6 + '</td><td>' + td7 + '</td><td>' + td8 + '</td><td>' + td9 + '</td><td>' + td10 + '</td><td>' + td11 + '</td><td>' + td12 + '</td><td>' + td13 + '</td></tr>');

            //CLEAR ALL VALUES
            $('#tbl-b1_name').val('');
            $('#tbl-b1-bday').val('');
            $('#tbl-b1_civstat').val('');
            $('#tbl-b1_edu').val('');
            $('#tbl-b1_occu').val('');
            $('#tbl-b1_income').val('');
            $('#tbl-b1_religion').val('');
            $('#tbl-b1_numbirthtotal').val('');
            $('#tbl-b1_numbirthalive').val('');
            $('#tbl-b1_residency').val('');
            $('#tbl-b1_tribe').val('');
        }
    });
    //B.2 Modal
    function showb2modal() {
        $('#modal-b2').modal('show', {
            backdrop: 'static'
        });
    }
    $("#btn-b2-add").click(function() {
        showb2modal();
        $('#tbl-b2_name').val('');
        $('#tbl-b2-bday').val('');
        $('#tbl-b2_civstat').val('');
        $('#tbl-b2_edu').val('');
        $('#tbl-b2_nutri').val('');
        $('#tbl-b2-features').val('');
        $('#tbl-b2_immu').val('');
        $('#tbl-b2_IsInSchool').val('');
    });
    //B.2 MODAL VALIDATION
    $("#b2form").validate();
    var b2isvalid;
    $("#tbl-b2_name").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b2-bday").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b2_nutri").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "Type 'None' if this person has no nutritional status specified.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-b2_civstat").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-b2_immu").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "Type 'None' if this person had no immunization.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-b2_edu").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "Type 'None' if this person had no educational background.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-b2-features").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "Type 'None' if this person has no special features.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-b2_IsInSchool").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "Type 'None' if no reason is stated.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-b2-save").click(function() {
        $("#b2form").valid();
        if ($("#b2form").valid() === true) {
            var td1 = $('#tbl-b2_name').val();
            var td2;
            if ($('#rd-b2-1-gender').is(':checked')) {
                td2 = "Male";
            }
            if ($('#rd-b2-2-gender').is(':checked')) {
                td2 = "Female";
            }
            var td3 = $('#tbl-b2-bday').val();
            var td4 = $('#tbl-b2_civstat').val();
            var td5 = $('#tbl-b2_edu').val();
            var td6 = $('#tbl-b2_nutri').val();
            var td7 = $('#tbl-b2_immu').val();
            var td8, td9;
            if ($('#rd-b2-1-school').is(':checked')) {
                td8 = "Yes";
                td9 = "None";
            }
            if ($('#rd-b2-2-school').is(':checked')) {
                td8 = "No";
                td9 = $('#tbl-b2_IsInSchool').val();
            }
            var td10 = $('#tbl-b2-features').val();

            var qn_num = $('#qn_num').val();
            var chld_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var b2 = $('#tbl-childstatus').data('kendoGrid');
            var b2datasource = b2.dataSource;
            b2datasource.insert({ CHLD_NUM: chld_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBER: qn_num, CHLD_NAME: td1, CHLD_SEX: td2, CHLD_BIRTHDATE: td3, CHLD_CIVSTATUS: td4, CHLD_HEALTHSTATUS: td6, CHLD_IMMUNE: td7, CHLD_SPECIALFEAT: td10, CHLD_EDU: td5, CHLD_ISINSCHOOL: td8, CHLD_REASON_NO_EDU: td9 });

            //CLEAR ALL VALUES
            $('#tbl-b2_name').val('');
            $('#tbl-b2-bday').val('');
            $('#tbl-b2_civstat').val('');
            $('#tbl-b2_edu').val('');
            $('#tbl-b2_nutri').val('');
            $('#tbl-b2-features').val('');
            $('#tbl-b2_immu').val('');
            $('#tbl-b2_IsInSchool').val('');
        }
    });
    //B.3 Modal
    function showb3modal() {
        $('#modal-b3').modal('show', {
            backdrop: 'static'
        });
    }
    $("#btn-b3-add").click(function() {
        showb3modal();
        $('#tbl-b3_item').val('');
        $('#tbl-b3_nonformal').val('');
        $('#tbl-b3_other').val('');
    });
    //B.3 MODAL VALIDATION
    $("#b3form").validate();
    var b3isvalid;
    $("#tbl-b3_item").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });

    $("#tbl-b3-save").click(function() {
        $("#b3form").valid();
        if ($("#b3form").valid() === true) {
            var td1 = $('#tbl-b3_item').val();
            var td2;
            if ($('#rd-b3-1-owned').is(':checked')) {
                td2 = "Yes";
            }
            if ($('#rd-b3-2-owned').is(':checked')) {
                td2 = "No";
            }
            var td3;
            if ($('#rd-b3-1-rented').is(':checked')) {
                td3 = "Yes";
            }
            if ($('#rd-b3-2-rented').is(':checked')) {
                td3 = "No";
            }
            var td4;
            if ($('#rd-b3-1-mortgage').is(':checked')) {
                td4 = "Yes";
            }
            if ($('#rd-b3-2-mortgage').is(':checked')) {
                td4 = "No";
            }
            var td5;
            if ($('#rd-b3-1-settler').is(':checked')) {
                td5 = "Yes";
            }
            if ($('#rd-b3-2-settler').is(':checked')) {
                td5 = "No";
            }
            var td6;
            if ($('#rd-b3-build-concrete').is(':checked')) {
                td6 = "Concrete";
            }
            if ($('#rd-b3-build-semiconcrete').is(':checked')) {
                td6 = "Semi-concrete";
            }
            if ($('#rd-b3-build-wood').is(':checked')) {
                td6 = "Wood";
            }
            if ($('#rd-b3-build-other').is(':checked')) {
                if($('#tbl-b3_other').val() === "" || $('#tbl-b3_other').val() === " ") {
                    $('#tbl-b3_other').val("None");
                }
            td6 = $('#tbl-b3_other').val();
            }
            var td10;
            if ($('#rd-b3-1-bedrm').is(':checked')) {
                td10 = "Yes";
            }
            if ($('#rd-b3-2-bedrm').is(':checked')) {
                td10 = "No";
            }
            var td11;
            if ($('#rd-b3-1-kitchn').is(':checked')) {
                td11 = "Yes";
            }
            if ($('#rd-b3-2-kitchn').is(':checked')) {
                td11 = "No";
            }
            var td12;
            if ($('#rd-b3-1-toilet').is(':checked')) {
                td12 = "Yes";
            }
            if ($('#rd-b3-2-toilet').is(':checked')) {
                td12 = "No";
            }
            var td13;
            if ($('#rd-b3-1-lvngrm').is(':checked')) {
                td13 = "Yes";
            }
            if ($('#rd-b3-2-lvngrm').is(':checked')) {
                td13 = "No";
            }

            var qn_num = $('#qn_num').val();
            var ownr_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var b3 = $('#tbl-houselndownr').data('kendoGrid');
            var b3datasource = b3.dataSource;
            b3datasource.insert({ OWNR_NUM: ownr_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, OWNRSHP_ITEM: td1, OWNRSHP_OWNED: td2, OWNRSHP_RENTED: td3, OWNRSHP_MORTGAGE: td4, OWNRSHP_INFRMAL_SETTLR: td5, OWNRSHP_HOUSEBUILD: td6, OWNRSHP_HASKITCHEN: td11, OWNRSHP_HASBEDRUM: td10, OWNRSHP_HASTOILET: td12, OWNRSHP_HASLIVNGRUM: td13 });

            //CLEAR ALL VALUES
            $('#tbl-b3_item').val('');
            $('#tbl-b3_nonformal').val('');
            $('#tbl-b3_other').val('');
        }
    });
    //C.1 Modal
    function showc1modal() {
        $('#modal-c1').modal('show', {
            backdrop: 'static'
        });
    }
    $("#btn-c1-add").click(function() {
        showc1modal();
        $('#tbl-c1_name').val('');
        $('#tbl-c1_ofwyears').val('');
        $('#tbl-c1_IsOFWOrgMember').val('');
    });
    //C.1 MODAL VALIDATION
    $("#c1form").validate();
    var c1isvalid;
    $("#tbl-c1_name").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-c1_ofwyears").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-c1_IsOFWOrgMember").rules("add", {
        required: true,
        minlength: 2,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters are necessary")
        }
    });
    $("#tbl-c1-save").click(function() {
        $("#c1form").valid();
        if ($("#c1form").valid() === true) {
            var td1 = $('#tbl-c1_name').val();
            var td2;
            if ($('#rd-c1-1-gender').is(':checked')) {
                td2 = "Male";
            }
            if ($('#rd-c1-2-gender').is(':checked')) {
                td2 = "Female";
            }
            var td3 = $('#tbl-c1_ofwyears').val();
            var td4, td5;
            if ($('#rd-c1-1-OFWOrgMember').is(':checked')) {
                td4 = "Yes";
                td5 = $('#tbl-c1_IsOFWOrgMember').val();
            }
            if ($('#rd-c1-2-OFWOrgMember').is(':checked')) {
                td4 = "No";
                td5 = "None";
            }

            var qn_num = $('#qn_num').val();
            var ofw_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var c1 = $('#tbl-ofw').data('kendoGrid');
            var c1datasource = c1.dataSource;
            c1datasource.insert({ OFW_NUM: ofw_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, OFW_NAME: td1, OFW_SEX: td2, OFW_YEARS_OVRSEAS: td3, OFW_ISMEMBEROFORG: td4, OFW_ORGNAME: td5 });

            //CLEAR ALL VALUES
            $('#tbl-c1_name').val('');
            $('#tbl-c1_ofwyears').val('');
            $('#tbl-c1_IsOFWOrgMember').val('');
        }
    });
    // In C.1 Modal
    $("#rd-OFWOrgMember-1-lbl").click(function() {
        $("#c1_IsOFWOrgMember_container").fadeIn();
        $("#c1_IsOFWOrgMember_container :input").prop('disabled', false);
    });
    $("#rd-OFWOrgMember-2-lbl").click(function() {
        $("#c1_IsOFWOrgMember_container").fadeOut();
        $("#c1_IsOFWOrgMember_container :input").prop('disabled', true);
    });
    $("#rd-c1-1-OFWOrgMember").click(function() {
        $("#c1_IsOFWOrgMember_container").fadeIn();
        $("#c1_IsOFWOrgMember_container :input").prop('disabled', false);
    });
    $("#rd-c1-2-OFWOrgMember").click(function() {
        $("#c1_IsOFWOrgMember_container").fadeOut();
        $("#c1_IsOFWOrgMember_container :input").prop('disabled', true);
    });
    //D.1 Modal
    function showd1modal() {
        $('#modal-d1').modal('show', {
            backdrop: 'static'
        });
    }
    $("#btn-d1-add").click(function() {
        showd1modal();
    });
    //D.1 MODAL VALIDATION
    $("#d1form").validate();
    var d1isvalid;
    $("#tbl-d1_name").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-d1_training").rules("add", {
        required: true,
        messages: {
            required: "Type 'None' if this person had no training.",
        }
    });
    $("#tbl-d1-save").click(function() {
        $("#d1form").valid();
        if ($("#d1form").valid() === true) {
            var td1 = $('#tbl-d1_name').val();
            var td2 = $('#tbl-d1_skills').val();
            var td3 = $('#tbl-d1_training').val();
            var td4, td5;
            if ($(td2).val() !== "") {
                if ($('#rd-d1-2-2betrained').is(':checked')) {
                    td2 = "None";
                    td3 = "None";
                    td4 = "No";
                    td5 = "None";
         //           $('#tbl-livelihoodsklls tr:last').after('<tr><td>' + td1 + '</td><td> None </td><td> None </td><td> No </td><td> None </td></tr>');
                } else {
                    td4 = "No";
                    td5 = "None";
         //           $('#tbl-livelihoodsklls tr:last').after('<tr><td>' + td1 + '</td><td>' + td2 + '</td><td>' + td3 + '</td><td> No </td><td> None </td></tr>');
                }
            }
            if ($('#rd-d1-1-2betrained').is(':checked')) {
                td2 = "None";
                td3 = "None";
                td4 = "Yes";
                td5 = $('#tbl-d1_preferskills').val();
         //       $('#tbl-livelihoodsklls tr:last').after('<tr><td>' + td1 + '</td><td> None </td><td> None </td><td> Yes </td><td>' + td5 + '</td></tr>');
            }

            var qn_num = $('#qn_num').val();
            var fam_skills_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var d1 = $('#tbl-livelihoodsklls').data('kendoGrid');
            var d1datasource = d1.dataSource;
            d1datasource.insert({ FAM_SKILLS_NUM: fam_skills_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, WORKFAMLY_NAME: td1, WORKFAMLY_SKILLS: td2, WORKFAMLY_TRAINEDHERE: td3, WORKFAMLY_ISWILLINGTOTRAIN: td4, WORKFAMLY_SKILLSPREFER: td5 });

            //CLEAR ALL VALUES
            $('#tbl-d1_name').val('');
            $('#tbl-d1_skills').val('');
            $('#tbl-d1_training').val('');
            $('#tbl-d1_preferskills').val('');
        }
    });
    $("#tbl-d1_skills").keyup(function() {
        if ($(this).val() !== "" || $(this).val() !== "None" || $(this).val() !== "none" || $(this).val() !== "NONE") {
            $('#rd-d1-2-2betrained').prop('checked', false);
            $("#d1_2betrained_container").fadeOut();
            $("#d1_training_container").fadeIn();
        }
        if ($(this).val() === "" || $(this).val() === "None" || $(this).val() === "none" || $(this).val() === "NONE") {
            $("#d1_2betrained_container").fadeIn();
            $("#d1_training_container").fadeOut();
        }
    });
    $("#rd-2betrained-1-lbl").click(function() {
        $("#d1_preferskills_container").fadeIn();
        $("#d1_preferskills_container :input").prop('disabled', false);
    });
    $("#rd-2betrained-2-lbl").click(function() {
        $("#d1_preferskills_container").fadeOut();
        $("#d1_preferskills_container :input").prop('disabled', true);
    });
    $("#rd-d1-1-2betrained").click(function() {
        $("#d1_preferskills_container").fadeIn();
        $("#d1_preferskills_container :input").prop('disabled', false);
    });
    $("#rd-d1-2-2betrained").click(function() {
        $("#d1_preferskills_container").fadeOut();
        $("#d1_preferskills_container :input").prop('disabled', true);
    });

    //E.1 Modal
    function showe1modal() {
        $('#modal-e1').modal('show', {
            backdrop: 'static'
        });
    }

    $("#btn-e1-add").click(function() {
        showe1modal();
    });
    //E.1 MODAL VALIDATION
    $("#e1form").validate();
    var e1isvalid;
    $("#tbl-e1_name").rules("add", {
        required: true,
        minlength: 3,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#e1_1-why").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required. Type none if no reason is specified.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#e1_2-why").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required. Type none if no reason is specified.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#e1_3-why").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required. Type none if no reason is specified.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-e1-save").click(function() {
        $("#e1form").valid();
        if ($("#e1form").valid() === true) {
            var td1 = $('#tbl-e1_name').val();
            var td2;
            if ($('#rd-e1-1-baptism').is(':checked')) {
                td2 = "Yes";
            }
            if ($('#rd-e1-2-baptism').is(':checked')) {
                td2 = "No";
            }
            var td3;
            if ($('#rd-e1-1-ksp').is(':checked')) {
                td3 = "Regularly";
            }
            if ($('#rd-e1-2-ksp').is(':checked')) {
                td3 = "Sometimes";
            }
            if ($('#rd-e1-3-ksp').is(':checked')) {
                td3 = "None";
            }
            var td4;
            if ($('#rd-e1-1-confirm').is(':checked')) {
                td4 = "Yes";
            }
            if ($('#rd-e1-2-confirm').is(':checked')) {
                td4 = "No";
            }
            var td5,td6;
            if ($('#rd-e1-1-marriage').is(':checked')) {
                td5 = "Yes";
                td6 = "Civil";
            }
            if ($('#rd-e1-2-marriage').is(':checked')) {
                td5 = "Yes";
                td6 = "Church";
            }
            if ($('#rd-e1-3-marriage').is(':checked')) {
                td5 = "No";
                td6 = "None";
            }
            var td7;
            if ($('#rd-e1-1-crchattndnce').is(':checked')) {
                td7 = "Every Sunday";
            }
            if ($('#rd-e1-2-crchattndnce').is(':checked')) {
                td7 = "Sometimes";
            }
            if ($('#rd-e1-3-crchattndnce').is(':checked')) {
                td7 = "None";
            }
            var td8;
            if ($('#rd-e1-1-confession').is(':checked')) {
                td8 = "Once a year";
            }
            if ($('#rd-e1-2-confession').is(':checked')) {
                td8 = "Sometimes";
            }
            if ($('#rd-e1-3-confession').is(':checked')) {
                td8 = "None";
            }
            var td9,td10;
            if ($('#rd-e1-1-baptism').is(':checked')) {
                td9 = "Yes";
                td10 = "None";
            }
            if ($('#rd-e1-2-baptism').is(':checked')) {
                td9 = "No";
                td10 = $('#e1_1-why').val();
            }
            var td11,td12;
            if ($('#rd-e1-1-confirm').is(':checked')) {
                td11 = "Yes";
                td12 = "None";
            }
            if ($('#rd-e1-2-confirm').is(':checked')) {
                td11 = "No";
                td12 = $('#e1_2-why').val();
            }
            var td13,td14;
            if ($('#rd-e1-1-marriage').is(':checked')) {
                td13 = "No";
                td14 = $('#e1_3-why').val();
            }
            if ($('#rd-e1-3-marriage').is(':checked')) {
                td13 = "No";
                td14 = $('#e1_3-why').val();
            }
            if ($('#rd-e1-2-marriage').is(':checked')) {
                td13 = "Yes";
                td14 = "None";
            }


            var qn_num = $('#qn_num').val();
            var fam_church_sacrmnt_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var e1 = $('#tbl-churchsac').data('kendoGrid');
            var e1datasource = e1.dataSource;
            e1datasource.insert({ FAM_CHURCH_SACRMNT_NUM: fam_church_sacrmnt_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, CHRCHFAMLY_NAME: td1, CHRCHFAMLY_ISBAPTIZED: td2, CHRCHFAMLY_KSP_ATTNDCE: td3, CHRCHFAMLY_CONFIRMED: td4, CHRCHFAMLY_ISMARRIED: td5, CHRCHFAMLY_MARRIEDHOW: td6, CHRCHFAMLY_CHRCH_ATTNDNCE: td7, CHRCHFAMLY_CONFSSN_FREQ: td8, CHRCHFAMLY_ISNOTBAPTIZED: td9, CHRCHFAMLY_REASN_NONBAPTIZED: td10, CHRCHFAMLY_ISNOTCONFSSN: td11, CHRCHFAMLY_REASN_NONCONFSSN: td12, CHRCHFAMLY_ISNOTMARRDINCHRCH: td13, CHRCHFAMLY_REASN_NONMARRDINCHR: td14 });

            //CLEAR ALL VALUES
            $('#tbl-e1_name').val('');
            $('#tbl-e1_chrchattndnce').val('');
            $('#tbl-e1_confession').val('');
            $('#e1_1-why').val('');
            $('#e1_2-why').val('');
            $('#e1_3-why').val('');
        }
    });


    //E.2 Modal
    function showe2modal() {
        $('#modal-e2').modal('show', {
            backdrop: 'static'
        });
    }
    $("#btn-e2-add").click(function() {
        showe2modal();
    });
    //E.2 MODAL VALIDATION
    $("#e2form").validate();
    var e2isvalid;
    $("#tbl-e2_name").rules("add", {
        required: true,
        minlength: 3,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-e2_others").rules("add", {
        required: true,
        minlength: 3,
        messages: {
            required: "Type none if it is not specified.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-e2-save").click(function() {
        $("#e2form").valid();
        if ($("#e2form").valid() === true) {
            var td1 = $('#tbl-e2_name').val();
            var td2;
            if ($('#rd-e2-1-evangelization').is(':checked')) {
                td2 = "Yes";
            }
            if ($('#rd-e2-2-evangelization').is(':checked')) {
                td2 = "No";
            }
            var td3;
            if ($('#rd-e2-1-gkk').is(':checked')) {
                td3 = "Yes";
            }
            if ($('#rd-e2-2-gkk').is(':checked')) {
                td3 = "No";
            }
            var td4;
            if ($('#rd-e2-1-bible').is(':checked')) {
                td4 = "Yes";
            }
            if ($('#rd-e2-2-bible').is(':checked')) {
                td4 = "No";
            }
            var td5;
            if ($('#rd-e2-1-liturgy').is(':checked')) {
                td5 = "Yes";
            }
            if ($('#rd-e2-2-liturgy').is(':checked')) {
                td5 = "No";
            }
            var td6;
            if ($('#rd-e2-1-social').is(':checked')) {
                td6 = "Yes";
            }
            if ($('#rd-e2-2-social').is(':checked')) {
                td6 = "No";
            }
            var td7;
            if ($('#rd-e2-1-eucharist').is(':checked')) {
                td7 = "Yes";
            }
            if ($('#rd-e2-2-eucharist').is(':checked')) {
                td7 = "No";
            }
            var td8;
            if ($('#tbl-e2_others').val() !== "") {
                td8 = $('#tbl-e2_others').val();
            } else {
                td8 = "None";
            }

            var qn_num = $('#qn_num').val();
            var fam_church_particpte_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var e2 = $('#tbl-e2_1').data('kendoGrid');
            var e2datasource = e2.dataSource;
            e2datasource.insert({ FAM_CHURCH_PARTICPTE_NUM: fam_church_particpte_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, CHRCHPART_NAME: td1, CHRCHPART_EVANGELIC: td2, CRCHPART_GKKORIENT: td3, CRCHPART_BSCBIBLE: td4, CRCHPART_BSCLITRGY: td5, CRCHPART_SOCTEACH: td6, CRCHPART_EUCHLIFE: td7, CRCHPART_OTHER: td8 });

            //CLEAR ALL VALUES
            $('#tbl-e2_name').val('');
            $('#tbl-e2_others').val('');
        }
    });
    //E.3 Modal
    function showe3modal() {
        $('#modal-e3').modal('show', {
            backdrop: 'static'
        });
    }
    $("#btn-e3-add").click(function() {
        showe3modal();
    });
    //E.3 MODAL VALIDATION
    $("#e3form").validate();
    var e3isvalid;
    $("#tbl-e3_title").rules("add", {
        required: true,
        messages: {
            required: "This field is required."
        }
    });
    $("#tbl-e3_topic").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-e3_conduct").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-e3_date").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-e3-save").click(function() {
        $("#e3form").valid();
        if ($("#e3form").valid() === true) {
            var td1 = $('#tbl-e3_title').val();
            var td2 = $('#tbl-e3_topic').val();
            var td3 = $('#tbl-e3_date').val();
            var td4 = $('#tbl-e3_conduct').val();

            var qn_num = $('#qn_num').val();
            var semnr_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var e3 = $('#tbl-e3_1').data('kendoGrid');
            var e3datasource = e3.dataSource;
            e3datasource.insert({ SEMNR_NUM: semnr_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, SEMNR_NAME: td1, SEMNR_TOPIC: td2, SEMNR_DATE: td3, SEMNR_CONDUCTD: td4 });

            //CLEAR ALL VALUES
            $('#tbl-e3_title').val('');
            $('#tbl-e3_topic').val('');
            $('#tbl-e3_date').val('');
            $('#tbl-e3_conduct').val('');
        }
    });

        //F.1 Modal
    $("#btn-f1-add").click(function() {
        $('#modal-f1').modal('show', {
            backdrop: 'static'
        });
    });
    //E.3 MODAL VALIDATION
    $("#f1form").validate();
    var f1isvalid;
    $("#tbl-f1_feedback").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-f1-save").click(function() {
        $("#f1form").valid();
        if ($("#f1form").valid() === true) {
            var td1 = $('#tbl-f1_feedback').val();
            var qn_num = $('#qn_num').val();
            var feedback_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var f1 = $('#tbl-f1').data('kendoGrid');
            var f1datasource = f1.dataSource;
            f1datasource.insert({ FEEDBACK_NUM: feedback_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, FEEDBACK: td1 });
            //CLEAR ALL VALUES
            $('#tbl-f1_feedback').val('');
         }
    });

        //F.2 Modal
    $("#btn-f2-add").click(function() {
        $('#modal-f2').modal('show', {
            backdrop: 'static'
        });
    });
    //E.3 MODAL VALIDATION
    $("#f2form").validate();
    var f2isvalid;
    $("#tbl-f2_feedback").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-f2-save").click(function() {
        $("#f2form").valid();
        if ($("#f2form").valid() === true) {
            var td1 = $('#tbl-f2_feedback').val();
            var qn_num = $('#qn_num').val();
            var feedback_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var f2 = $('#tbl-f2').data('kendoGrid');
            var f2datasource = f2.dataSource;
            f2datasource.insert({ FEEDBACK_NUM: feedback_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, FEEDBACK: td1 });
            //CLEAR ALL VALUES
            $('#tbl-f2_feedback').val('');
         }
    });

        //F.3 Modal
    $("#btn-f3-add").click(function() {
        $('#modal-f3').modal('show', {
            backdrop: 'static'
        });
    });
    //E.3 MODAL VALIDATION
    $("#f3form").validate();
    var f3isvalid;
    $("#tbl-f3_feedback").rules("add", {
        required: true,
        minlength: 4,
        messages: {
            required: "This field is required.",
            minlength: jQuery.format("At least {0} characters is required.")
        }
    });
    $("#tbl-f3-save").click(function() {
        $("#f3form").valid();
        if ($("#f3form").valid() === true) {
            var td1 = $('#tbl-f3_feedback').val();
            var qn_num = $('#qn_num').val();
            var feedback_num = chance.natural({min: 1, max: 99999999});
            var survyr_num = $('#survyr_num').val();
            var rspondnt_num = $('#rspondnt_num').val();
            var f3 = $('#tbl-f3').data('kendoGrid');
            var f3datasource = f3.dataSource;
            f3datasource.insert({ FEEDBACK_NUM: feedback_num, RSPONDNT_NUM: rspondnt_num, SURVYR_NUM: survyr_num , QN_NUMBR: qn_num, FEEDBACK: td1 });
            //CLEAR ALL VALUES
            $('#tbl-f3_feedback').val('');
         }
    });
 /*   //F.1 Textboxes
    function showerrmsgF() {
        var msg = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.error("There are no more entries to remove!", "Error:", msg);
    }

    function showinfomsgF() {
        var msg = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.info("You can only have 7 entries!", "Note:", msg);
    }
    var ctrf1 = 2;
    $("#btn-f1-add").click(function() {
        if (ctrf1 > 7) {
            showinfomsgF();
            return false;
        }
        $("#f1_textboxgroup").append('<input type="text" class="form-control input-lg" name="f1_why1' + ctrf1 + '" id="f1_txtbox' + ctrf1 + '" placeholder="Please state a reason">');
        ctrf1++;
    });
    $("#btn-f1-del").click(function() {
        if (ctrf1 == 1) {
            showerrmsgF();
            return false;
        }
        ctrf1--;
        $("#f1_txtbox" + ctrf1).remove();
    });
    //F.2 Textboxes
    var ctrf2 = 2;
    $("#btn-f2-add").click(function() {
        if (ctrf2 > 7) {
            showinfomsgF();
            return false;
        }
        $("#f2_textboxgroup").append('<input type="text" class="form-control input-lg" name="f2_why' + ctrf2 + '" id="f2_txtbox' + ctrf2 + '" placeholder="Please state a reason">');
        ctrf2++;
    });
    $("#btn-f2-del").click(function() {
        if (ctrf2 == 1) {
            showerrmsgF();
            return false;
        }
        ctrf2--;
        $("#f2_txtbox" + ctrf2).remove();
    });
    //F.3 Textboxes
    var ctrf3 = 2;
    $("#btn-f3-add").click(function() {
        if (ctrf3 > 7) {
            showinfomsgF();
            return false;
        }
        $("#f3_textboxgroup").append('<input type="text" class="form-control input-lg" name="f3_why' + ctrf3 + '" id="f3_txtbox' + ctrf3 + '" placeholder="Please state a reason">');
        ctrf3++;
    });
    $("#btn-f3-del").click(function() {
        if (ctrf3 == 1) {
            showerrmsgF();
            return false;
        }
        ctrf3--;
        $("#f3_txtbox" + ctrf3).remove();
    });

    */

    //Reports Page restrictions
    $("#reports_page").click(function() {

        var usertype = $('#typeuser').html();
        if (usertype != "SuperUser") {
            $('#reports_access_modal').modal('show', {
            backdrop: 'static'
            });
        }
        else {
            setTimeout(function() {document.location.href='/reports';}, 500);
        }
    });
    //Account Statistics
    $("#account_stats").click(function() {
        $("#account_stats_form").submit();
        $('#modal-accountstats').modal('show', {
                backdrop: 'static'
            });
    });

    setTimeout(function() {
          $("#account_stats_form").submit();
    }, 1000);

    $("#account_stats_form").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "/wizard/tables/backend/account_stats.php",
            data: $(this).serialize(),
            dataType: 'text',
            success: function(data) {
                $( "#num_records_account" ).attr( "data-end", data );
            }
       });

    });
    //DATA SUBMIT MODAL
    function showsubmitmodal() {
    $('#modal-submitall').modal('show', {
            backdrop: 'static'
        });
    }

    $("#btn-confirm").click(function() {
        showsubmitmodal();
    });

    function showclosingmodal() {
    $('#modal-confirmclose').modal('show', {
            backdrop: 'static'
        });
    }

    $("#btn_submit_all").click(function() {
        var zone_num = $('#respondent_gkk :selected').val();
        $( "#zone_num" ).attr( "value", zone_num );


        var interview;
            if ($('#rd-a0-1-interview').is(':checked')) {
                interview = "First";
            }
            if ($('#rd-a0-2-interview').is(':checked')) {
                interview = "Second";
            }
        $('#interview_value').val(interview);
        var gender;
            if ($('#rd-a1-1-gender').is(':checked')) {
                gender = "Male";
            }
            if ($('#rd-a1-2-gender').is(':checked')) {
                gender = "Female";
            }
        $('#gender').val(gender);

        $( "#rootwizard" ).submit();

        var b1 = $('#tbl-fmlyhousehld').data('kendoGrid');
        var b1datasource = b1.dataSource;
        b1datasource.sync();

        console.log("B1 data synced!");

        var b2 = $('#tbl-childstatus').data('kendoGrid');
        var b2datasource = b2.dataSource;
        b2datasource.sync();

        console.log("B2 data synced!");

        var b3 = $('#tbl-houselndownr').data('kendoGrid');
        var b3datasource = b3.dataSource;
        b3datasource.sync();

        console.log("B3 data synced!");

        if ($('#rd-ofw-1').is(':checked')) {
            var c1 = $('#tbl-ofw').data('kendoGrid');
            var c1datasource = c1.dataSource;
            c1datasource.sync();

            console.log("C1 data synced!");
        }

        var d1 = $('#tbl-livelihoodsklls').data('kendoGrid');
        var d1datasource = d1.dataSource;
        d1datasource.sync();

        console.log("D1 data synced!");

        var e1 = $('#tbl-churchsac').data('kendoGrid');
        var e1datasource = e1.dataSource;
        e1datasource.sync();

        console.log("E1 data synced!");

        if ($('#rd-e2_1-1').is(':checked')) {
            var e2 = $('#tbl-e2_1').data('kendoGrid');
            var e2datasource = e2.dataSource;
            e2datasource.sync();

            console.log("E2 data synced!");
        }

        if ($('#rd-e3_1-1').is(':checked')) {
            var e3 = $('#tbl-e3_1').data('kendoGrid');
            var e3datasource = e3.dataSource;
            e3datasource.sync();

            console.log("E3 data synced!");
        }

        var f1 = $('#tbl-f1').data('kendoGrid');
        var f1datasource = f1.dataSource;
        f1datasource.sync();

        console.log("F1 data synced!");

        var f2 = $('#tbl-f2').data('kendoGrid');
        var f2datasource = f2.dataSource;
        f2datasource.sync();

        console.log("F2 data synced!");

        var f3 = $('#tbl-f3').data('kendoGrid');
        var f3datasource = f3.dataSource;
        f3datasource.sync();

        console.log("F3 data synced!");

        $('#modal-submitall').modal('hide');
        showclosingmodal();
    });

    $("#btn_confirm_close").click(function() {
        $("body").fadeOut("normal");
        setTimeout(function() {document.location.href='/logout';}, 500);
    });

    $("#btn_confirm_new").click(function() {
        $("#account_stats_form").submit();
        $("body").fadeOut("normal");
        setTimeout(function() {document.location.href='/wizard';}, 500);
    });


});