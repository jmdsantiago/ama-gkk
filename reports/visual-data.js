$(document).ready(function() {

function getRandomInt(min, max) 
{
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

	// Baptized Chart
	var donut_chart_demo = $("#donut-chart-baptized");
	
	donut_chart_demo.parent().show();
	
	var donut_chart = Morris.Donut({
		element: 'donut-chart-baptized',
		data: [
			{label: "Download Sales", value: getRandomInt(10,50)},
			{label: "In-Store Sales", value: getRandomInt(10,50)},
			{label: "Mail-Order Sales", value: getRandomInt(10,50)}
		],
		colors: ['#707f9b', '#455064', '#242d3c']
	});
	
	donut_chart_demo.parent().attr('style', '');
	
	// Married Chart
	var donut_chart_demo2 = $("#donut-chart-married");
	
	donut_chart_demo2.parent().show();
	
	var donut_chart2 = Morris.Donut({
		element: 'donut-chart-married',
		data: [
			{label: "Download Sales", value: getRandomInt(10,50)},
			{label: "In-Store Sales", value: getRandomInt(10,50)},
			{label: "Mail-Order Sales", value: getRandomInt(10,50)}
		],
		colors: ['#707f9b', '#455064', '#242d3c']
	});
	
	donut_chart_demo2.parent().attr('style', '');
	
	// Confirmed Chart
	var donut_chart_demo3 = $("#donut-chart-confirmed");
	
	donut_chart_demo3.parent().show();
	
	var donut_chart3 = Morris.Donut({
		element: 'donut-chart-confirmed',
		data: [
			{label: "Download Sales", value: getRandomInt(10,50)},
			{label: "In-Store Sales", value: getRandomInt(10,50)},
			{label: "Mail-Order Sales", value: getRandomInt(10,50)}
		],
		colors: ['#707f9b', '#455064', '#242d3c']
	});
	
	donut_chart_demo3.parent().attr('style', '');
	

});