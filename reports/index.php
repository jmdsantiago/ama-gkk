<?php
session_start();
if(!isset($_SESSION['usr_num'])) {
	header("Location: /login");
}
$name = $_SESSION['usersname'];
$usrtype = $_SESSION['usertype'];
if ($usrtype != "SuperUser") {
	echo  "<script type=\"text/javascript\">alert(\"I don\'t know how you got here but you\'re not getting through $name!\");</script>";
	header("Location: /");
}
include $_SERVER['DOCUMENT_ROOT'] . '/top_page_comment.html';
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<script src="/assets/preloader/pace.min.js"></script>
		<link rel="stylesheet" href="/assets/preloader/pace.css">
		<meta charset="utf-8">
		<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<meta name="description" content="Classified. Access denied." />
		<meta name="author" content="John Santiago" />
		<title>Survey Data | 001A</title>
		<?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/important_scripts.php'; ?>
		<link rel="stylesheet" type="text/css" href="/assets/flipclock/flipclock.css">
		<script src="/assets/flipclock/flipclock.js"></script>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
	<script type="text/javascript">
	$(document).ready(function() {
		var clock = $('.clock').FlipClock({
			clockFace: 'TwelveHourClock'
		});
	var d = new Date();

	var month = d.getMonth()+1;
	var day = d.getDate();

	var output = d.getFullYear() + '/' +
	    ((''+month).length<2 ? '0' : '') + month + '/' +
	    ((''+day).length<2 ? '0' : '') + day;

	$('#currentdate').append(output);
	});
	</script>
	<body class="page-body page-fade-only">
		<div class="page-container horizontal-menu">
			<!-- SIDEBAR -->
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/header.php'; ?>
			<div class="main-content">
				<!--PLACE ALERTS HERE-->
				<ol class="breadcrumb bc-3">
					<li>
						<a href="/"><i class="entypo-home"></i>Home</a>
					</li>
					<li class="active">
						<strong>Survey Data</strong>
					</li>
				</ol>
				<br/>
				<div class="row">
					<div class="col-md-8">
						<div class="well">
							<div class="clock"></div>
							<h3>&nbsp; &nbsp; Welcome to the site <strong><?php echo $_SESSION['usersname']; ?></strong>.</h3>
							<h3 id="currentdate">&nbsp; &nbsp; DATE: </h3>
						</div>
					</div>
					<div class="col-md-4 well">
						<h1>Choose a report:</h1>
						<select id="report_mode" name="report_mode" class="select2" data-allow-clear="true" data-placeholder="Select a report style">
						    <optgroup label="By Barangay">
						        <option value="2A">Barangay 2-A</option>
						        <option value="5A">Barangay 5-A</option>
						        <option value="6A">Barangay 6-A</option>
						        <option value="7A">Barangay 7-A</option>
						        <option value="8A">Barangay 8-A</option>
						        <option value="9A">Barangay 9-A</option>
						    </optgroup>
						    <optgroup label="Other">
						        <option>Overall</option>
						    </optgroup>
						</select>
						<br>
        				<button type="button" id="btn_report_mode" class="btn btn-primary"><i class="entypo-plus"></i>View</button>
					</div>
				</div>

				<div id="per-barangay">
				<div class="row">
					<div class="col-md-3">
						<div class="tile-stats tile-red">
							<div class="icon"><i class="entypo-users"></i></div>
							<div class="num" data-start="0" data-end="83" data-postfix="" data-duration="1500" data-delay="0">0</div>

							<h3>Respondents</h3>
							<p>are registered in the database.</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="tile-stats tile-aqua">
							<div class="icon"><i class="entypo-mail"></i></div>
							<div class="num" data-start="0" data-end="23" data-postfix="" data-duration="1200" data-delay="0">0</div>

							<h3>Surveys</h3>
							<p>have been conducted so far.</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="tile-stats tile-red">
							<div class="icon"><i class="entypo-users"></i></div>
							<div class="num" data-start="0" data-end="83" data-postfix="" data-duration="1500" data-delay="0">0</div>

							<h3>Surveyors</h3>
							<p>are registered in the database.</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="tile-stats tile-aqua">
							<div class="icon"><i class="entypo-mail"></i></div>
							<div class="num" data-start="0" data-end="23" data-postfix="" data-duration="1200" data-delay="0">0</div>

							<h3>Entries</h3>
							<p>you have entered into the database so far.</p>
						</div>
					</div>
				</div>
				<!--
				<hr>
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-primary" id="charts_env">
							<div class="panel-heading">
								<div class="panel-title">Demographics I</div>

								<div class="panel-options">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#pie-chart-demogr1" data-toggle="tab">Baptized</a></li>
										<li class=""><a href="#pie-chart-demogr2" data-toggle="tab">Married</a></li>
										<li class=""><a href="#pie-chart-demogr3" data-toggle="tab">Confirmed</a></li>
									</ul>
								</div>
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane active" id="pie-chart-demogr1">
										<div id="donut-chart-baptized" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr2">
										<div id="donut-chart-married" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr3">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-8">
						<div class="panel panel-primary" id="charts_env">
							<div class="panel-heading">
								<div class="panel-title">Demographics II</div>

								<div class="panel-options">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#pie-chart-demogr1" data-toggle="tab">Educational Attainment</a></li>
										<li class=""><a href="#pie-chart-demogr2" data-toggle="tab">Civil Status</a></li>
										<li class=""><a href="#pie-chart-demogr3" data-toggle="tab">Religion</a></li>
										<li class=""><a href="#pie-chart-demogr4" data-toggle="tab">Monthly Income</a></li>
										<li class=""><a href="#pie-chart-demogr5" data-toggle="tab">Births</a></li>
									</ul>
								</div>
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane active" id="pie-chart-demogr1">
										<div id="donut-chart-baptized" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr2">
										<div id="donut-chart-married" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr3">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr4">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr5">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-primary" id="charts_env">
							<div class="panel-heading">
								<div class="panel-title">Attendance Frequency (Church Sacraments)</div>

								<div class="panel-options">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#pie-chart-demogr1" data-toggle="tab">KSP</a></li>
										<li class=""><a href="#pie-chart-demogr2" data-toggle="tab">Eucharistic Celebration</a></li>
										<li class=""><a href="#pie-chart-demogr3" data-toggle="tab">Confession</a></li>
									</ul>
								</div>
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane active" id="pie-chart-demogr1">
										<div id="donut-chart-baptized" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr2">
										<div id="donut-chart-married" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr3">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-8">
						<div class="panel panel-primary" id="charts_env">
							<div class="panel-heading">
								<div class="panel-title">Attendance (Church Seminars / Trainings)</div>

								<div class="panel-options">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#pie-chart-demogr1" data-toggle="tab">Evangelization</a></li>
										<li class=""><a href="#pie-chart-demogr2" data-toggle="tab">GKK Orientation</a></li>
										<li class=""><a href="#pie-chart-demogr3" data-toggle="tab">Basic Bible</a></li>
										<li class=""><a href="#pie-chart-demogr4" data-toggle="tab">Basic Liturgy</a></li>
										<li class=""><a href="#pie-chart-demogr2" data-toggle="tab">Social Teaching</a></li>
										<li class=""><a href="#pie-chart-demogr3" data-toggle="tab">Life in the Eucharist</a></li>
										<li class=""><a href="#pie-chart-demogr4" data-toggle="tab">Others</a></li>
									</ul>
								</div>
							</div>
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane active" id="pie-chart-demogr1">
										<div id="donut-chart-baptized" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr2">
										<div id="donut-chart-married" class="morrischart" style="height: 300px"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr3">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
									<div class="tab-pane" id="pie-chart-demogr4">
										<div id="donut-chart-confirmed" class="morrischart" style="height: 300px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				-->
				</div>
			</div>
			<?php include $_SERVER['DOCUMENT_ROOT'] . '/footer.php'; ?>
		</div>
<?php include $_SERVER['DOCUMENT_ROOT'] . '/assets/afterload_scripts.php'; ?>
<script src="/reports/visual-data.js"></script>
	</body>
</html>